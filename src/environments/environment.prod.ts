export const environment = {
  production: true,
  refreshDelay: 300,
  useHttps: false,
  crytonRESTApiHost: '127.0.0.1',
  crytonRESTApiPort: 8000
};
