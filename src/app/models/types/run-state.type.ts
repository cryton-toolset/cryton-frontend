export type RunState =
  | 'PENDING'
  | 'STARTING'
  | 'RUNNING'
  | 'FINISHED'
  | 'IGNORE'
  | 'PAUSING'
  | 'PAUSED'
  | 'ERROR'
  | 'TERMINATED'
  | 'TERMINATING'
  | 'WAITING'
  | 'AWAITING'
  | 'SCHEDULED';
