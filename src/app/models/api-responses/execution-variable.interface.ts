export interface ExecutionVariable {
  id: number;
  created_at: string;
  updated_at: string;
  name: string;
  value: string;
  plan_execution: number;
}
