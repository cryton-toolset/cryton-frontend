export interface TableData<T> {
  count: number;
  results: T[];
}
