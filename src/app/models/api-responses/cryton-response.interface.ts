export interface CrytonResponse<T> {
  count: number;
  results: T[];
}
