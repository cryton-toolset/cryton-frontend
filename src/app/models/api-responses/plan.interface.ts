export interface Plan {
  id: number;
  created_at: string;
  updated_at: string;
  name: string;
  owner: string;
  plan: object;
}
