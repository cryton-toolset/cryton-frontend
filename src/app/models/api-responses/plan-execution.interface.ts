export interface PlanExecution {
  id: number;
  created_at: string;
  updated_at: string;
  state: string;
  start_time: string;
  pause_time: string;
  finish_time: string;
  schedule_time: string;
  aps_job_id: number;
  evidence_dir: string;
  run: number;
  plan_model: number;
  worker: number;
}
