/**
 * Expected description of main template dependency graph.
 */
export const httpTemplateDescription = `plan:
  name: HTTP trigger plan
  owner: Test runner
  stages:
    - name: stage-one
      trigger_type: delta
      trigger_args:
        hours: 0
        minutes: 0
        seconds: 5
      steps:
        - name: get-request
          step_type: worker/execute
          arguments:
            module: mod_cmd
            module_arguments:
              cmd: curl http://localhost:8082/index?a=1
          is_init: true
    - name: stage-two
      trigger_type: HTTPListener
      trigger_args:
        host: localhost
        port: 8082
        routes:
          - path: /index
            method: GET
            parameters:
              - name: a
                value: "1"
      steps:
        - name: scan-localhost
          step_type: worker/execute
          arguments:
            module: mod_nmap
            module_arguments:
              target: 127.0.0.1
          is_init: true
      depends_on:
        - stage-one
`;
