import { Worker } from '../../models/api-responses/worker.interface';

export const workers: Worker[] = [
  {
    id: 1,
    name: 'Worker 1',
    description: 'Description 1',
    created_at: '2022-03-12T11:30:49.842301Z',
    updated_at: '2022-03-12T11:30:49.842301Z',
    state: 'UP'
  },
  {
    id: 2,
    name: 'Worker 2',
    description: 'Description 2',
    created_at: '2022-03-12T11:30:49.842301Z',
    updated_at: '2022-03-12T11:30:49.842301Z',
    state: 'DOWN'
  },
  {
    id: 3,
    name: 'Worker 3',
    description: 'Description 3',
    created_at: '2022-03-12T11:30:49.842301Z',
    updated_at: '2022-03-12T11:30:49.842301Z',
    state: 'READY'
  }
];
