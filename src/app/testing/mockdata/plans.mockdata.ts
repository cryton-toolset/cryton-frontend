import { Plan } from '../../models/api-responses/plan.interface';

export const plans: Plan[] = [
  {
    id: 2,
    created_at: '2020-08-24T12:22:55.909039',
    updated_at: '2020-08-24T12:22:56.024864',
    name: 'Example scenario',
    owner: 'your name',
    plan: {}
  },
  {
    id: 3,
    created_at: '2020-08-24T12:22:56.873167',
    updated_at: '2020-08-24T12:22:57.597889',
    name: 'Example scenario',
    owner: 'your name',
    plan: {}
  },
  {
    id: 4,
    created_at: '2020-08-24T12:23:01.564407',
    updated_at: '2020-08-24T12:23:01.705385',
    name: 'Example scenario',
    owner: 'your name',
    plan: {}
  }
];
