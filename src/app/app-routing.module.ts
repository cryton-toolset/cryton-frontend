import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationComponent } from './core/navigation/navigation.component';
import { PageUnavailableComponent } from './core/page-unavailable/page-unavailable.component';
import { DashboardPageComponent } from './modules/dashboard/pages/dashboard-page/dashboard-page.component';

const routes: Routes = [
  {
    path: 'app',
    component: NavigationComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardPageComponent
      },
      {
        path: 'user',
        loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule)
      },
      { path: 'runs', loadChildren: () => import('./modules/run/run.module').then(m => m.RunModule) },
      { path: 'workers', loadChildren: () => import('./modules/worker/worker.module').then(m => m.WorkerModule) },
      { path: 'plans', loadChildren: () => import('./modules/plan/plan.module').then(m => m.PlanModule) },
      {
        path: 'templates',
        loadChildren: () => import('./modules/template/template.module').then(m => m.TemplateModule)
      },
      { path: 'logs', loadChildren: () => import('./modules/log/log.module').then(m => m.LogModule) },
      { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
      { path: '**', component: PageUnavailableComponent }
    ]
  },
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) },
  { path: '', redirectTo: '/app/dashboard', pathMatch: 'full' },
  { path: '**', component: PageUnavailableComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
