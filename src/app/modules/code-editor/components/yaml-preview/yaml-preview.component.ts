import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { catchError, first, map, Observable, of, switchMap, tap } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { HasYaml } from 'src/app/models/interfaces/has-yaml.interface';

@Component({
  selector: 'app-yaml-preview',
  templateUrl: './yaml-preview.component.html'
})
export class YamlPreviewComponent implements OnInit {
  @Input() pluckArgs = ['detail', 'plan'];
  @Input() resourceService: HasYaml;
  @Input() itemName: string;
  @Input() keepLastArg = true;

  yaml$: Observable<Record<string, unknown> | null>;
  itemID: number;

  constructor(private _route: ActivatedRoute, private _alert: AlertService) {}

  ngOnInit(): void {
    this.fetchYaml();
  }

  fetchYaml(): void {
    this.yaml$ = this._route.params.pipe(
      first(),
      tap((params: Params) => (this.itemID = Number(params['id']))),
      switchMap((params: Params) => this.resourceService.fetchYaml(params['id'] as number)),
      map(x => {
        let result: unknown = x;
        this.pluckArgs.forEach(arg => (result = result[arg]));
        return result;
      }),
      map(yaml => {
        if (this.keepLastArg) {
          const lastProperty = this.pluckArgs[this.pluckArgs.length - 1];
          const result = {};
          result[lastProperty] = yaml;
          return result;
        }
        return yaml;
      }),
      catchError(() => {
        this._alert.showError('Fetching YAML failed.');
        return of(null);
      })
    ) as Observable<Record<string, unknown> | null>;
  }
}
