import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  DoCheck,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  KeyValueDiffer,
  KeyValueDiffers,
  NgZone,
  OnDestroy,
  Optional,
  Output,
  Self,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NgControl, Validators } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import * as CodeMirror from 'codemirror';
import { Editor, EditorFromTextArea, ScrollInfo } from 'codemirror';
import { Subject } from 'rxjs';

function normalizeLineEndings(str: string): string {
  if (!str) {
    return str;
  }
  return str.replace(/\r\n|\r/g, '\n');
}

@Component({
  selector: 'app-code-editor-textarea',
  templateUrl: './code-editor-textarea.component.html',
  styleUrls: ['./code-editor-textarea.component.scss'],
  providers: [{ provide: MatFormFieldControl, useExisting: CodeEditorTextareaComponent }],
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeEditorTextareaComponent
  implements MatFormFieldControl<string>, OnDestroy, ControlValueAccessor, AfterViewInit, DoCheck
{
  @HostBinding() id = `example-tel-input-${CodeEditorTextareaComponent.nextId++}`;
  @ViewChild('ref') ref!: ElementRef<HTMLTextAreaElement>;

  static nextId = 0;

  codeMirror?: EditorFromTextArea;

  /**
   * Implemented as part of MatFormFieldControl.
   */
  stateChanges = new Subject<void>();

  /**
   * Implemented as part of MatFormFieldControl.
   */
  focused = false;

  /**
   * Implemented as part of MatFormFieldControl.
   */
  controlType = 'code-editor-textarea';

  /**
   * Implemented as part of MatFormFieldControl.
   */
  ariaDescribedByIds = '';

  /**
   * Implemented as part of MatFormFieldControl.
   */
  value = '';

  /**
   * Implemented as part of MatFormFieldControl.
   * Doesn't work, can be implemented with CodeMirror addon.
   */
  @Input()
  get placeholder() {
    return this._placeholder;
  }
  set placeholder(value) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  @Input()
  get required(): boolean {
    return this._required ?? this.ngControl?.control?.hasValidator(Validators.required) ?? false;
  }
  set required(value: BooleanInput) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  @Input()
  get disabled(): boolean {
    if (this.ngControl && this.ngControl.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }
  set disabled(value: BooleanInput) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /* name applied to the created textarea */
  @Input() name = 'codemirror';

  /* autofocus setting applied to the created textarea */
  @Input() autoFocus = false;

  /**
   * set options for codemirror
   * @link http://codemirror.net/doc/manual.html#config
   */
  @Input()
  set options(value: { [key: string]: unknown }) {
    this._options = value;
    if (!this._differ && value) {
      this._differ = this._differs.find(value).create();
    }
  }

  /* preserve previous scroll position after updating value */
  @Input() preserveScrollPosition = false;

  /* called when the text cursor is moved */
  @Output() cursorActivity = new EventEmitter<Editor>();

  /* called when the editor is focused or loses focus */
  @Output() focusChange = new EventEmitter<boolean>();

  /* called when the editor is scrolled */
  @Output() editorScrolled = new EventEmitter<ScrollInfo>();

  /* called when file(s) are dropped */
  @Output() fileDropped = new EventEmitter<[Editor, DragEvent]>();

  /**
   * Implemented as part of MatFormFieldControl.
   */
  get empty(): boolean {
    return this.value === '';
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  get errorState(): boolean {
    return this.ngControl.errors !== null && this.ngControl.touched === true && !this.focused;
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  private _placeholder = '';
  private _disabled = false;
  private _options: Record<string, unknown> = {
    theme: 'transparent',
    mode: 'yaml',
    lineNumbers: true,
    extraKeys: {
      Tab: (cm: CodeMirror) => {
        const spaces = Array(cm.getOption('indentUnit') + 1).join(' ');
        cm.replaceSelection(spaces);
      }
    }
  };
  private _differ?: KeyValueDiffer<string, unknown>;
  private _required?: boolean;

  constructor(
    @Optional() @Self() public ngControl: NgControl,
    private _differs: KeyValueDiffers,
    private _ngZone: NgZone
  ) {
    if (this.ngControl != null) {
      // Setting the value accessor directly (instead of using
      // the providers) to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }

  ngAfterViewInit() {
    this._ngZone.runOutsideAngular(async () => {
      this.codeMirror = CodeMirror.fromTextArea(this.ref.nativeElement, this._options) as EditorFromTextArea;

      this.codeMirror.on('cursorActivity', cm => this._ngZone.run(() => this.cursorActive(cm)));
      this.codeMirror.on('scroll', this.scrollChanged.bind(this));
      this.codeMirror.on('blur', () => this._ngZone.run(() => this.focusChanged(false)));
      this.codeMirror.on('focus', () => this._ngZone.run(() => this.focusChanged(true)));
      this.codeMirror.on('change', cm => this._ngZone.run(() => this.codemirrorValueChanged(cm)));
      this.codeMirror.on('drop', (cm, e) => {
        this._ngZone.run(() => this.dropFiles(cm, e));
      });

      this.codeMirror.setValue(this.value);
    });
  }

  ngDoCheck() {
    if (!this._differ) {
      return;
    }

    // check options have not changed
    const changes = this._differ.diff(this._options);
    if (changes) {
      changes.forEachChangedItem(option => this.setOptionIfChanged(option.key, option.currentValue));
      changes.forEachAddedItem(option => this.setOptionIfChanged(option.key, option.currentValue));
      changes.forEachRemovedItem(option => this.setOptionIfChanged(option.key, option.currentValue));
    }
  }

  ngOnDestroy(): void {
    this.stateChanges.complete();

    if (this.codeMirror) {
      this.codeMirror.toTextArea();
    }
  }

  codemirrorValueChanged(cm: Editor) {
    const cmVal = cm.getValue();

    if (this.value !== cmVal) {
      this.value = cmVal;
      this._onChange(this.value);
      this.stateChanges.next();
    }
  }

  setOptionIfChanged(optionName: string, newValue: unknown) {
    // Either set the option directly in CodeMirror or pre-set it in options before view init.
    if (this.codeMirror) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.codeMirror.setOption(optionName as any, newValue);
    } else {
      this._options[optionName] = newValue;
    }
  }

  focusChanged(focused: boolean) {
    this._onTouched();
    this.focused = focused;
    this.focusChange.emit(focused);
    this.stateChanges.next();
  }

  scrollChanged(cm: Editor) {
    this.editorScrolled.emit(cm.getScrollInfo());
  }

  cursorActive(cm: Editor) {
    this.cursorActivity.emit(cm);
  }

  dropFiles(cm: Editor, e: DragEvent) {
    this.fileDropped.emit([cm, e]);
  }

  /** Focuses the input. */
  focus(options?: FocusOptions): void {
    this.ref.nativeElement.focus(options);
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  setDescribedByIds(ids: string[]) {
    this.ariaDescribedByIds = ids.join(' ');
  }

  /**
   * Implemented as part of MatFormFieldControl.
   */
  onContainerClick(): void {
    if (!this.focused) {
      this.focus();
    }
  }

  /**
   * Implemented as part of ControlValueAccessor.
   */
  writeValue(value: string) {
    if (value === null || value === undefined) {
      return;
    }

    if (!this.codeMirror) {
      this.value = value;
      return;
    }

    const cur = this.codeMirror.getValue();

    if (value !== cur && normalizeLineEndings(cur) !== normalizeLineEndings(value)) {
      this.value = value;
      if (this.preserveScrollPosition) {
        const prevScrollPosition = this.codeMirror.getScrollInfo();
        this.codeMirror.setValue(this.value);
        this.codeMirror.scrollTo(prevScrollPosition.left, prevScrollPosition.top);
      } else {
        this.codeMirror.setValue(this.value);
      }
    }
  }

  /**
   * Implemented as part of ControlValueAccessor.
   */
  registerOnChange(fn: (value: string) => void) {
    this._onChange = fn;
  }

  /**
   * Implemented as part of ControlValueAccessor.
   */
  registerOnTouched(fn: () => void) {
    this._onTouched = fn;
  }

  /**
   * Implemented as part of ControlValueAccessor.
   */
  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    this.setOptionIfChanged('readOnly', this.disabled ? 'nocursor' : false);
    this.stateChanges.next();
  }

  /**
   * Implemented as part of ControlValueAccessor.
   */
  private _onChange: (value: string) => void;

  /**
   * Implemented as part of ControlValueAccessor.
   */
  private _onTouched = () => {};
}
