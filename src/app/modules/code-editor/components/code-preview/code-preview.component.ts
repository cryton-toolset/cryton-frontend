import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-code-preview',
  templateUrl: 'code-preview.component.html',
  styleUrls: ['code-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodePreviewComponent {
  @Input() code: string;
  @Input() showCopyButton = true;

  readonly editorOptions = { lineNumbers: true, mode: 'yaml', readOnly: 'nocursor', theme: 'transparent' };

  constructor() {}
}
