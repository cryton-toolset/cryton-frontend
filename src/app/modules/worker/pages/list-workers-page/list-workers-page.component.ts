import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { ActionButton } from 'src/app/shared/components/cryton-table/buttons/action-button';
import { DeleteButton } from 'src/app/shared/components/cryton-table/buttons/delete-button';
import { HealthCheckButton } from 'src/app/shared/components/cryton-table/buttons/healthcheck-button';
import { CrytonTableComponent } from 'src/app/shared/components/cryton-table/cryton-table.component';
import { WorkerTableDataSource } from 'src/app/shared/components/cryton-table/data-source/worker-table.data-source';

@Component({
  selector: 'app-list-workers-page',
  templateUrl: './list-workers-page.component.html',
  animations: [renderComponentTrigger]
})
export class ListWorkersPageComponent implements OnInit {
  @ViewChild(CrytonTableComponent) table: CrytonTableComponent<Worker>;

  dataSource: WorkerTableDataSource;
  buttons: ActionButton<Worker>[];

  constructor(private _workersService: WorkersService, private _dialog: MatDialog) {}

  ngOnInit(): void {
    this.dataSource = new WorkerTableDataSource(this._workersService);
    this.buttons = [new HealthCheckButton(this._workersService), new DeleteButton(this._workersService, this._dialog)];
  }
}
