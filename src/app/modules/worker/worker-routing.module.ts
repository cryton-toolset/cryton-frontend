import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateWorkerPageComponent } from './pages/create-worker-page/create-worker-page.component';
import { ListWorkersPageComponent } from './pages/list-workers-page/list-workers-page.component';

const routes: Routes = [
  { path: 'list', component: ListWorkersPageComponent },
  { path: 'create', component: CreateWorkerPageComponent },
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: '/app/workers/list'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkerRoutingModule {}
