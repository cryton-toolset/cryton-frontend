import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../../shared/shared.module';
import { CreateWorkerPageComponent } from './pages/create-worker-page/create-worker-page.component';
import { ListWorkersPageComponent } from './pages/list-workers-page/list-workers-page.component';
import { WorkerRoutingModule } from './worker-routing.module';

@NgModule({
  declarations: [ListWorkersPageComponent, CreateWorkerPageComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatButtonModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    WorkerRoutingModule
  ]
})
export class WorkerModule {}
