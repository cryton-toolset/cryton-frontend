import { Component, Type } from '@angular/core';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { StepOverviewItem } from 'src/app/shared/components/cryton-editor/models/step-overview-item.interface';
import { StepType } from 'src/app/shared/components/cryton-editor/models/step-type.enum';
import { TemplateUploadStepsComponent } from 'src/app/shared/components/cryton-editor/steps/template-upload-steps/template-upload-steps.component';

@Component({
  selector: 'app-upload-template-page',
  templateUrl: './upload-template-page.component.html',
  animations: [renderComponentTrigger]
})
export class UploadTemplatePageComponent {
  uploadTemplateSteps: Type<TemplateUploadStepsComponent> = TemplateUploadStepsComponent;
  stepOverviewItems: StepOverviewItem[] = [{ name: 'Template Upload', type: StepType.SELECTABLE, required: true }];

  constructor() {}
}
