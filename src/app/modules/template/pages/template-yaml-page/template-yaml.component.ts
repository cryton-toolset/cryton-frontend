import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateService } from 'src/app/core/http/template/template.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';

@Component({
  selector: 'app-template-yaml',
  template:
    '<app-yaml-preview [resourceService]="templateService" itemName="template" [pluckArgs]="pluckArgs"></app-yaml-preview>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateYamlPageComponent {
  pluckArgs = ['detail', 'plan'];

  constructor(
    public templateService: TemplateService,
    protected _route: ActivatedRoute,
    protected _alert: AlertService
  ) {}
}
