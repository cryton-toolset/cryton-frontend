import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListTemplatesPageComponent } from './pages/list-templates-page/list-templates-page.component';
import { TemplateYamlPageComponent } from './pages/template-yaml-page/template-yaml.component';
import { UploadTemplatePageComponent } from './pages/upload-template-page/upload-template-page.component';

const routes: Routes = [
  { path: 'list', component: ListTemplatesPageComponent },
  {
    path: 'create',
    loadChildren: () => import('../template-creator/template-creator.module').then(m => m.TemplateCreatorModule)
  },
  { path: 'upload', component: UploadTemplatePageComponent },
  { path: ':id/yaml', component: TemplateYamlPageComponent },
  {
    path: '',
    redirectTo: '/app/templates/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule {}
