import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CodeEditorModule } from '../code-editor/code-editor.module';
import { TemplateCreatorModule } from '../template-creator/template-creator.module';
import { CreatePlanPageComponent } from './pages/create-plan-page/create-plan-page.component';
import { ListPlansPageComponent } from './pages/list-plans-page/list-plans-page.component';
import { PlanGraphPageComponent } from './pages/plan-graph-page/plan-graph-page.component';
import { PlanYamlPageComponent } from './pages/plan-yaml-page/plan-yaml-page.component';
import { PlanRoutingModule } from './plan-routing.module';

@NgModule({
  declarations: [ListPlansPageComponent, CreatePlanPageComponent, PlanYamlPageComponent, PlanGraphPageComponent],
  imports: [CommonModule, SharedModule, PlanRoutingModule, CodeEditorModule, TemplateCreatorModule]
})
export class PlanModule {}
