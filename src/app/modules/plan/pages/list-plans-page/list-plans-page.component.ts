import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PlanService } from 'src/app/core/http/plan/plan.service';
import { Plan } from 'src/app/models/api-responses/plan.interface';
import { DeleteButton } from 'src/app/shared/components/cryton-table/buttons/delete-button';
import { LinkButton } from 'src/app/shared/components/cryton-table/buttons/link-button';
import { TableButton } from 'src/app/shared/components/cryton-table/buttons/table-button';
import { CrytonTableComponent } from 'src/app/shared/components/cryton-table/cryton-table.component';
import { PlanTableDataSource } from 'src/app/shared/components/cryton-table/data-source/plan-table.data-source';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';

@Component({
  selector: 'app-list-plans-page',
  templateUrl: './list-plans-page.component.html'
})
export class ListPlansPageComponent implements OnInit {
  @ViewChild(CrytonTableComponent) table: CrytonTableComponent<Plan>;

  dataSource: PlanTableDataSource;
  buttons: TableButton[];
  filesToUpload: FileList;

  constructor(private _planService: PlanService, private _dialog: MatDialog, private _datePipe: CrytonDatetimePipe) {}

  ngOnInit(): void {
    this.dataSource = new PlanTableDataSource(this._planService, this._datePipe);
    this.buttons = [
      new LinkButton('Open graph', 'visibility_on', '/app/plans/:id/graph'),
      new LinkButton('Show YAML', 'description', '/app/plans/:id/yaml'),
      new DeleteButton(this._planService, this._dialog)
    ];
  }
}
