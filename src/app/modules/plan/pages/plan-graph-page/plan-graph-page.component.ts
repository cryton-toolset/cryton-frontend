import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first, map, mergeMap, Observable, Subject, takeUntil } from 'rxjs';
import { PlanService } from 'src/app/core/http/plan/plan.service';
import { PlanDescription } from 'src/app/modules/template-creator/models/interfaces/template-description';

@Component({
  selector: 'app-plan-graph-page',
  templateUrl: './plan-graph-page.component.html',
  styleUrls: ['./plan-graph-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanGraphPageComponent implements OnDestroy, OnInit {
  planDescription: PlanDescription;

  private _destroy = new Subject<void>();

  constructor(private _route: ActivatedRoute, private _planService: PlanService, private _cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this._fetchPlanOnIdChange();
  }

  ngOnDestroy(): void {
    this._destroy.next();
    this._destroy.complete();
  }

  private _fetchPlanDescription(planId: number): Observable<PlanDescription> {
    return this._planService.fetchYaml(planId).pipe(
      first(),
      map(res => res.detail)
    );
  }

  private _fetchPlanOnIdChange(): void {
    this._route.params
      .pipe(
        takeUntil(this._destroy),
        mergeMap(params => this._fetchPlanDescription(params.id))
      )
      .subscribe(planDescription => {
        this.planDescription = planDescription;
        this._cd.detectChanges();
      });
  }
}
