import { Component } from '@angular/core';
import { StepOverviewItem } from 'src/app/shared/components/cryton-editor/models/step-overview-item.interface';
import { StepType } from 'src/app/shared/components/cryton-editor/models/step-type.enum';
import { PlansCreationStepsComponent } from 'src/app/shared/components/cryton-editor/steps/plans-creation-steps/plans-creation-steps.component';

@Component({
  selector: 'app-create-plan-page',
  templateUrl: './create-plan-page.component.html'
})
export class CreatePlanPageComponent {
  editorSteps = PlansCreationStepsComponent;
  stepOverviewItems: StepOverviewItem[] = [
    { name: 'Select Template', type: StepType.SELECTABLE, required: true },
    { name: 'Upload Inventories', type: StepType.SELECTABLE, required: false }
  ];

  constructor() {}
}
