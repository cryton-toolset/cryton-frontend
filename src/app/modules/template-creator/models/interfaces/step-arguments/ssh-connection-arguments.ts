export interface SshConnectionArguments {
  target: string;
  username?: string;
  password?: string;
  ssh_key?: string;
  port?: number;
}
