export interface StepArgumentsComponent<T> {
  /**
   * Returns step arguments.
   */
  getArguments: () => T;

  /**
   * Fills arguments form with arguments.
   *
   * @param args Step arguments.
   */
  fill(args: T): void;

  /**
   * @returns True if arguments form is valid.
   */
  isValid(): boolean;
}
