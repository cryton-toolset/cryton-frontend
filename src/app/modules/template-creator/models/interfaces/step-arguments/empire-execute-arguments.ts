export interface EmpireExecuteArguments {
  use_agent: string;
  shell_command?: string;
  module?: string;
  module_arguments?: object;
}
