import { SshConnectionArguments } from './ssh-connection-arguments';

export interface SessionArguments {
  use_named_session?: string;
  use_any_session_to_target?: string;
  session_id?: number;
  ssh_connection?: SshConnectionArguments;
}
