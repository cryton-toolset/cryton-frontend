import { SessionArguments } from './session-arguments';

export interface EmpireAgentDeployArguments extends SessionArguments {
  listener_name: string;
  listener_port?: number;
  listener_options?: object;
  listener_type?: string;
  stager_type?: string;
  stager_options?: object;
  agent_name: string;
}
