export interface MSFArgs {
  identifiers?: MSFIdentifiers;
  exploit: string;
  payload: string;
  exploit_arguments?: Record<string, string>;
  payload_arguments?: Record<string, string>;
}

export interface MSFIdentifiers {
  type?: string;
  tunnel_local?: string;
  tunnel_peer?: string;
  via_exploit?: string;
  via_payload?: string;
  desc?: string;
  info?: string;
  workspace?: string;
  session_host?: string;
  session_port?: string;
  target_host?: string;
  username?: string;
  uuid?: string;
  exploit_uuid?: string;
  routes?: string;
  arch?: string;
}
