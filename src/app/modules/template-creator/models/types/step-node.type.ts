import { EmpireAgentDeployArguments } from '../interfaces/step-arguments/empire-agent-deploy-arguments';
import { EmpireExecuteArguments } from '../interfaces/step-arguments/empire-execute-arguments';
import { WorkerExecuteArguments } from '../interfaces/step-arguments/worker-execute-arguments';

export type StepArgumentsType = WorkerExecuteArguments | EmpireExecuteArguments | EmpireAgentDeployArguments;
