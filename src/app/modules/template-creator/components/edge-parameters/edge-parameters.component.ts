import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { SelectionOption } from 'src/app/shared/models/interfaces/selection-option.interface';
import { ShortStringPipe } from 'src/app/shared/pipes/short-string.pipe';
import { StepEdge } from '../../classes/dependency-graph/edge/step-edge';
import { EdgeCondition } from '../../models/interfaces/edge-condition';

// Type of edge's value form
type EdgeValueFormType = FormGroup<{ value: FormControl<string> }>;

// Type of edge's condition
type Condition = { type: FormControl<string>; valueForm?: EdgeValueFormType };

// Type of FormArray of conditions
type ConditionsArrayType = FormGroup<Condition>;

@Component({
  selector: 'app-edge-parameters',
  templateUrl: './edge-parameters.component.html',
  styleUrls: ['./edge-parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EdgeParametersComponent implements OnInit, OnDestroy {
  destroy$ = new Subject<void>();
  conditionsFormGroup = new FormGroup({
    conditions: new FormArray<ConditionsArrayType>([])
  });

  typeOptions: SelectionOption<string>[] = [
    { value: 'result', name: 'RESULT' },
    { value: 'output', name: 'OUTPUT' },
    { value: 'serialized_output', name: 'SERIALIZED_OUTPUT' },
    { value: 'any', name: 'ANY' }
  ];

  resultOptions: string[] = ['OK', 'FAIL'];
  invalidError = 'Invalid conditions.';

  constructor(
    private _alert: AlertService,
    private _dialogRef: MatDialogRef<EdgeParametersComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { edge: StepEdge }
  ) {}

  get conditions(): FormArray<ConditionsArrayType> {
    return this.conditionsFormGroup.controls.conditions;
  }

  ngOnInit(): void {
    this._loadEdgeConditions(this.data.edge);
    this._createAfterClosedSub();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Closes the dialog window.
   */
  close(): void {
    this._dialogRef.close();
  }

  /**
   * Saves edge data and closes the dialog window.
   */
  saveEdge(): void {
    if (this.conditionsFormGroup.valid) {
      this.data.edge.conditions = this.getEdgeConditions();

      this.close();
    } else {
      this._alert.showError('Conditions are invalid.');
    }
  }

  /**
   * Adds new empty condition form group to the conditions array.
   *
   * @param type Type of condition.
   * @returns Condition FormGroup.
   */
  addCondition(type: string = null): FormGroup {
    const condition = new FormGroup<Condition>({
      type: new FormControl(type, [Validators.required])
    });

    if (type !== 'any') {
      condition.addControl('valueForm', this._createValueForm(type));
    }

    this.conditions.push(condition);
    return condition;
  }

  /**
   * Removes the given condition from the conditions array.
   *
   * @param index Index of the condition to remove.
   */
  removeCondition(index: number): void {
    if (this.conditions.length > 1) {
      this.conditions.removeAt(index);
    } else {
      this._alert.showError('Step edge must contain at least 1 condition.');
    }
  }

  /**
   * Gets edge's parent node name.
   *
   * @returns Parent node name.
   */
  getParentName(): string {
    const name = this.data.edge?.parentNode.name ?? '';
    return new ShortStringPipe().transform(name, 10);
  }

  /**
   * Gets edge's child node name.
   *
   * @returns Child node name.
   */
  getChildName(): string {
    const name = this.data.edge?.childNode.name ?? '';
    return new ShortStringPipe().transform(name, 10);
  }

  onConditionTypeChange(index: number, type: string): void {
    const condition = this.conditions.at(index);
    const valueForm = this._createValueForm(type);
    const currentValueForm = condition.get('valueForm');

    if (currentValueForm) {
      if (valueForm) {
        condition.setControl('valueForm', valueForm);
      } else {
        condition.removeControl('valueForm');
      }
    } else {
      if (valueForm) {
        condition.addControl('valueForm', valueForm);
      }
    }
  }

  getEdgeConditions(): EdgeCondition[] {
    const conditions = this.conditions.value;
    const edgeConditions: EdgeCondition[] = [];

    conditions.forEach(condition => {
      if (condition.type === 'result') {
        const valueForm = condition.valueForm;
        edgeConditions.push({ type: condition.type, value: valueForm.value });
      } else if (condition.type === 'any') {
        edgeConditions.push({ type: condition.type });
      } else {
        const valueForm = condition.valueForm;
        edgeConditions.push({
          type: condition.type,
          value: valueForm.value
        });
      }
    });

    return edgeConditions;
  }

  /**
   * Subscribes to the dialog reference's afterClosed observable
   * for destroying an invalid edge (edge with 0 conditions) after closing.
   */
  private _createAfterClosedSub(): void {
    this._dialogRef
      .afterClosed()
      .pipe(first())
      .subscribe(() => {
        if (this.data.edge.conditions.length === 0) {
          this.data.edge.destroy();
        }
      });
  }

  /**
   * Loads all edge conditions and fills the form fields with them.
   * At least one condition form group must be always present.
   *
   * @param edge Step edge from MAT_DIALOG_DATA.
   */
  private _loadEdgeConditions(edge: StepEdge): void {
    edge.conditions.forEach(condition => {
      const conditionForm = this.addCondition(condition.type);

      if (condition.type === 'result') {
        conditionForm.get('valueForm').get('value').setValue(condition.value);
      } else if (condition.type !== 'any') {
        const value = condition.value;
        conditionForm.get('valueForm').setValue({ value });
      }
    });

    if (edge.conditions.length === 0) {
      this.addCondition();
    }
  }

  private _createValueForm(type: string): EdgeValueFormType | null {
    switch (type) {
      case 'result':
        return new FormGroup({
          value: new FormControl('OK', [Validators.required])
        });
      case 'any':
        return null;
      default:
        return new FormGroup({
          value: new FormControl('', [Validators.required])
        });
    }
  }
}
