import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { SharedModule } from 'src/app/shared/shared.module';
import { StageForm } from '../../classes/stage-creation/forms/stage-form';
import { StageParametersComponent } from './stage-parameters.component';

describe('StageParametersComponent', () => {
  let component: StageParametersComponent;
  let fixture: ComponentFixture<StageParametersComponent>;

  const testStageForm = new StageForm();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StageParametersComponent, ComponentInputDirective],
      imports: [
        SharedModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatInputModule,
        BrowserAnimationsModule
      ]
    })
      .overrideComponent(StageParametersComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StageParametersComponent);
    component = fixture.componentInstance;
    component.stageForm = testStageForm;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
