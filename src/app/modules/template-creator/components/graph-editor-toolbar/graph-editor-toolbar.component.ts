import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DependencyGraphEditor } from '../../classes/dependency-graph/dependency-graph-editor';
import { ToolsService } from '../../services/tools.service';

interface Tool {
  name: string;
  icon: string;
  action: () => void;
  active?: () => boolean;
}

@Component({
  selector: 'app-graph-editor-toolbar',
  templateUrl: './graph-editor-toolbar.component.html',
  styleUrls: ['./graph-editor-toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GraphEditorToolbarComponent {
  @Input() graphEditor: DependencyGraphEditor;

  tools: Tool[] = [
    { name: 'Zoom in', icon: 'zoom_in', action: (): void => this._toolsService.zoomIn(this.graphEditor) },
    { name: 'Zoom out', icon: 'zoom_out', action: (): void => this._toolsService.zoomOut(this.graphEditor) },
    {
      name: 'Fit inside screen',
      icon: 'fullscreen',
      action: (): void => this._toolsService.fitScreen(this.graphEditor)
    },
    {
      name: 'Enable moving nodes',
      icon: 'back_hand',
      action: (): void => this._toolsService.enableNodeMove(this.graphEditor),
      active: (): boolean => this.graphEditor.toolState.isMoveNodeEnabled
    },
    {
      name: 'Enable swapping nodes',
      icon: 'swap_horizontal',
      action: (): void => this._toolsService.enableSwap(this.graphEditor),
      active: (): boolean => this.graphEditor.toolState.isSwapEnabled
    },
    {
      name: 'Enable deleting nodes',
      icon: 'delete_forever',
      action: (): void => this._toolsService.enableDelete(this.graphEditor),
      active: (): boolean => this.graphEditor.toolState.isDeleteEnabled
    }
  ];

  constructor(private _toolsService: ToolsService) {}
}
