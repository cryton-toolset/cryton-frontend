import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { stringify } from 'yaml';
import { DependencyGraphOverview } from '../../classes/dependency-graph/dependency-graph-overview';
import { GraphViewManager } from '../../classes/dependency-graph/graph-view-manager';
import { OverviewCompositeNodeConfigurer } from '../../classes/dependency-graph/node/overview-composite-node-configurer';
import { NodeType } from '../../models/enums/node-type';
import { PlanDescription, StageDescription, StepDescription } from '../../models/interfaces/template-description';
import { PlanImporterService } from '../../services/plan-importer.service';

@Component({
  selector: 'app-plan-overview',
  templateUrl: './plan-overview.component.html',
  styleUrls: ['./plan-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanOverviewComponent implements OnInit, AfterViewInit {
  @Input() planDescription: PlanDescription;

  planOverview = new DependencyGraphOverview();
  NodeType = NodeType;
  maxTooltipTextLength = 30;

  private _graphViewManager = new GraphViewManager();

  constructor(private _planImporter: PlanImporterService) {}

  ngOnInit(): void {
    const nodeConfigurer = new OverviewCompositeNodeConfigurer(this.planOverview);

    this._importPlan(this.planDescription);
    this.planOverview.draw();
    this.planOverview.configureEvents(nodeConfigurer);
  }

  ngAfterViewInit(): void {
    // Must be called after view init to ensure that konva stage was created.
    this.fitScreen();
  }

  fitScreen(): void {
    this._graphViewManager.fitScreen(this.planOverview, this.planOverview.depGraph);
  }

  zoomIn(): void {
    this._graphViewManager.zoomIn(this.planOverview);
  }

  zoomOut(): void {
    this._graphViewManager.zoomOut(this.planOverview);
  }

  asStageDescription(data: StageDescription | StepDescription): StageDescription {
    return data as StageDescription;
  }

  asStepDescription(data: StageDescription | StepDescription): StepDescription {
    return data as StepDescription;
  }

  stageArgsToYAML(stage: StageDescription): string {
    return stringify(stage.trigger_args).trim();
  }

  stepArgsToYaml(step: StepDescription): string {
    const { name, is_init, next, step_type, arguments: args, ...rest } = step;
    return stringify({ ...args, ...rest }).trim();
  }

  private _importPlan(planDescription: PlanDescription): void {
    this._planImporter.importPlan(this.planOverview.depGraph, planDescription);
  }
}
