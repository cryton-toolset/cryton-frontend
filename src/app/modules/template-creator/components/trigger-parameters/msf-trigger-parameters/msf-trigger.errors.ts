export const ERROR_MESSAGES: Record<string, Record<string, string>> = {
  exploit_arguments: {
    invalidYaml: 'YAML format invalid.'
  },
  payload_arguments: {
    invalidYaml: 'YAML format invalid.'
  }
};
