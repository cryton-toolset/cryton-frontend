import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CodeEditorModule } from 'src/app/modules/code-editor/code-editor.module';
import { EmpireExecuteArguments } from '../../../models/interfaces/step-arguments/empire-execute-arguments';
import { EmpireExecuteArgumentsComponent } from './empire-execute-arguments.component';

const MOCK_SHELL_CMD_ARGS: EmpireExecuteArguments = {
  use_agent: 'Agent name',
  shell_command: 'ls'
};

const MOCK_SHELL_EMPIRE_ARGS: EmpireExecuteArguments = {
  use_agent: 'Agent name',
  module: 'empire/module',
  module_arguments: { arg: 'value' }
};

describe('EmpireExecuteArgumentsComponent', () => {
  let component: EmpireExecuteArgumentsComponent;
  let fixture: ComponentFixture<EmpireExecuteArgumentsComponent>;
  let loader: HarnessLoader;

  const getFieldByLabel = async (floatingLabelText: string): Promise<MatFormFieldHarness> =>
    loader.getHarness(MatFormFieldHarness.with({ floatingLabelText })).catch(() => undefined);

  const getTypeSelection = async (): Promise<MatSelectHarness> => loader.getHarness(MatSelectHarness);

  const checkEmpireFieldsExistance = async (shouldExist: boolean): Promise<void> => {
    checkFieldsExistance(['Module *', 'Module arguments (in YAML)'], shouldExist);
  };

  const checkShellCmdFieldsExistance = async (shouldExist: boolean): Promise<void> => {
    checkFieldsExistance(['Shell command *'], shouldExist);
  };

  const checkFieldsExistance = async (fieldLabels: string[], shouldExist: boolean): Promise<void> => {
    const fields = await Promise.all(fieldLabels.map(label => getFieldByLabel(label)));
    fields.forEach(field => {
      if (shouldExist) {
        expect(field).toBeDefined();
      } else {
        expect(field).toBeUndefined();
      }
    });
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        NoopAnimationsModule,
        MatDividerModule,
        MatRadioModule,
        MatSelectModule,
        CodeEditorModule
      ],
      declarations: [EmpireExecuteArgumentsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpireExecuteArgumentsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should display default fields`, async () => {
    const defaultFieldLabels = ['Use agent *', 'Execution option *'];

    const fields = await Promise.all(defaultFieldLabels.map(label => getFieldByLabel(label)));
    fields.forEach(field => expect(field).toBeDefined());
  });

  it('should show shell command fields on shell command selection', async () => {
    // Should not be shown if no radio is checked
    await checkShellCmdFieldsExistance(false);

    const selection = await getTypeSelection();

    await selection.clickOptions({ text: 'Shell command' });
    fixture.detectChanges();

    // Should be shown after radio gets checked
    await checkShellCmdFieldsExistance(true);
  });

  it('should show empire module fields on empire module selection', async () => {
    // Should not be shown if no radio is checked
    await checkEmpireFieldsExistance(false);

    const selection = await getTypeSelection();
    await selection.clickOptions({ text: 'Empire module' });
    fixture.detectChanges();

    // Should be shown after radio gets checked
    await checkEmpireFieldsExistance(true);
  });

  it('should fill the form correctly with shell command arguments', async () => {
    component.fill(MOCK_SHELL_CMD_ARGS);
    fixture.detectChanges();
    const filledArgs = component.getArguments();

    expect(filledArgs).toEqual(MOCK_SHELL_CMD_ARGS);
  });

  it('should fill the form correctly with empire arguments', async () => {
    component.fill(MOCK_SHELL_EMPIRE_ARGS);
    fixture.detectChanges();

    expect(component.getArguments()).toEqual(MOCK_SHELL_EMPIRE_ARGS);
  });

  it('should be invalid if use agent is missing', async () => {
    // Fill every field except use_agent with a valid value.
    component.form.patchValue({
      shellCmdForm: { shell_command: 'some command' },
      empireModuleForm: { module: 'empire', module_arguments: 'arg: value\n' }
    });
    expect(component.isValid()).toBeFalse();
  });

  it('should be invalid if module arguments use invalid YAML', async () => {
    // Fill every field with a valid value, but use invalid yaml for module args.
    component.form.patchValue({
      use_agent: 'agent name',
      shellCmdForm: { shell_command: 'some command' },
      empireModuleForm: { module: 'empire', module_arguments: 'a: a:\n' }
    });
    expect(component.isValid()).toBeFalse();
  });

  it('should be invalid if all accessible fields are valid but no arguments option has been chosen', () => {
    component.form.get('use_agent').setValue('valid value');
    expect(component.isValid()).toBeFalse();
  });
});
