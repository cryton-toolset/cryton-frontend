import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SelectionOption } from 'src/app/shared/models/interfaces/selection-option.interface';
import { SessionArguments } from '../../../models/interfaces/step-arguments/session-arguments';
import { StepArgumentsComponent } from '../../../models/interfaces/step-arguments/step-arguments-component';
import { SshConnectionArgumentsComponent } from '../ssh-connection-arguments/ssh-connection-arguments.component';

enum SessionOption {
  NONE,
  NAMED_SESSION,
  ANY_SESSION_TO_TARGET,
  SESSION_ID,
  SSH_CONNECTION
}

@Component({
  selector: 'app-session-selection-arguments',
  templateUrl: './session-selection-arguments.component.html',
  styleUrls: ['../../../styles/step-form.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SessionSelectionArgumentsComponent implements StepArgumentsComponent<SessionArguments>, OnInit {
  @ViewChild(SshConnectionArgumentsComponent) sshConnectionArgs: SshConnectionArgumentsComponent;
  @Input() isRequired: boolean;

  readonly selectionOpts: SelectionOption<SessionOption>[] = [
    { value: SessionOption.NAMED_SESSION, name: 'Use named session' },
    { value: SessionOption.ANY_SESSION_TO_TARGET, name: 'Use any session to target' },
    { value: SessionOption.SESSION_ID, name: 'Use session ID' },
    { value: SessionOption.SSH_CONNECTION, name: 'Use SSH connection' }
  ];

  readonly selectionCtrl = new FormControl<number>(null);
  readonly useNamedSessionCtrl = new FormControl<string>(null, [Validators.required]);
  readonly useAnySessionToTargetCrl = new FormControl<string>(null, [Validators.required]);
  readonly sessionIdCtrl = new FormControl<number>(null, [Validators.required, Validators.pattern(/^[0-9]*$/)]);

  readonly SessionOption = SessionOption;

  constructor(private _cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.isRequired) {
      this.selectionCtrl.addValidators(Validators.required);
    } else {
      this.selectionOpts.unshift({ value: SessionOption.NONE, name: 'None' });
    }
  }

  getArguments(): SessionArguments {
    switch (this.selectionCtrl.value) {
      case SessionOption.ANY_SESSION_TO_TARGET:
        return { use_any_session_to_target: this.useAnySessionToTargetCrl.value };
      case SessionOption.NAMED_SESSION:
        return { use_named_session: this.useNamedSessionCtrl.value };
      case SessionOption.SESSION_ID:
        return { session_id: this.sessionIdCtrl.value };
      case SessionOption.SSH_CONNECTION:
        return { ssh_connection: this.sshConnectionArgs.getArguments() };
      default:
        return {};
    }
  }

  fill(args: SessionArguments): void {
    if (Object.values(args).filter(value => value).length > 1) {
      throw Error(`Session arguments can only define 1 type of session.`);
    } else if (args.session_id != null) {
      this.selectionCtrl.setValue(SessionOption.SESSION_ID);
      this.sessionIdCtrl.setValue(args.session_id);
    } else if (args.use_any_session_to_target != null) {
      this.selectionCtrl.setValue(SessionOption.ANY_SESSION_TO_TARGET);
      this.useAnySessionToTargetCrl.setValue(args.use_any_session_to_target);
    } else if (args.use_named_session != null) {
      this.selectionCtrl.setValue(SessionOption.NAMED_SESSION);
      this.useNamedSessionCtrl.setValue(args.use_named_session);
    } else if (args.ssh_connection != null) {
      this.selectionCtrl.setValue(SessionOption.SSH_CONNECTION);
      this._cd.detectChanges();
      this.sshConnectionArgs.fill(args.ssh_connection);
    } else {
      this.selectionCtrl.reset();
    }
  }

  isValid(): boolean {
    const selectedType = this.selectionCtrl.value;

    if (!selectedType || selectedType === SessionOption.NONE) {
      return !this.isRequired;
    } else if (selectedType === SessionOption.ANY_SESSION_TO_TARGET) {
      return this.useAnySessionToTargetCrl.valid;
    } else if (selectedType === SessionOption.NAMED_SESSION) {
      return this.useNamedSessionCtrl.valid;
    } else if (selectedType === SessionOption.SESSION_ID) {
      return this.sessionIdCtrl.valid;
    } else if (selectedType === SessionOption.SSH_CONNECTION) {
      return this.sshConnectionArgs?.isValid() ?? false;
    }
  }
}
