import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SshConnectionArguments } from '../../../models/interfaces/step-arguments/ssh-connection-arguments';
import { SshConnectionArgumentsComponent } from './ssh-connection-arguments.component';

const EXPECTED_FIELD_LABELS: string[] = ['Target *', 'Username', 'Password', 'SSH key', 'Port'];

const MOCK_FORM_VALUE: SshConnectionArguments = {
  target: '127.0.0.1',
  username: 'root',
  password: 'root',
  ssh_key: 'something',
  port: 80
};

describe('SshConnectionArgumentsComponent', () => {
  let component: SshConnectionArgumentsComponent;
  let fixture: ComponentFixture<SshConnectionArgumentsComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, MatInputModule, MatFormFieldModule, NoopAnimationsModule],
      declarations: [SshConnectionArgumentsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SshConnectionArgumentsComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create all fields available for SSH connection arguments', async () => {
    const fields = await loader.getAllHarnesses(MatFormFieldHarness);
    const fieldLabels = await Promise.all(fields.map(field => field.getLabel()));

    expect(fieldLabels.sort()).toEqual([...EXPECTED_FIELD_LABELS].sort());
  });

  it('target field should be required', async () => {
    const field = await loader.getHarness(MatFormFieldHarness.with({ floatingLabelText: 'Target *' }));
    expect(await field.getControl().then(field => field.isRequired())).toBeTrue();
  });

  it('should be invalid if required field is empty', () => {
    // We can check right away since form is empty by default.
    expect(component.isValid()).toBeFalse();
  });

  it('should be valid if all required fields are not empty', () => {
    component.form.get('target').setValue('something');
    expect(component.isValid()).toBeTrue();
  });

  it('should return arguments', () => {
    component.form.patchValue(MOCK_FORM_VALUE);
    expect(component.getArguments()).toEqual(MOCK_FORM_VALUE);
  });

  it('should fill the form', () => {
    component.fill(MOCK_FORM_VALUE);
    expect(component.getArguments()).toEqual(MOCK_FORM_VALUE);
  });
});
