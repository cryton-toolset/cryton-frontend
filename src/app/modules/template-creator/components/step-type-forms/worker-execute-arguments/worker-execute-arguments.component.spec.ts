import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CodeEditorModule } from 'src/app/modules/code-editor/code-editor.module';
import { stringify } from 'yaml';
import { WorkerExecuteArguments } from '../../../models/interfaces/step-arguments/worker-execute-arguments';
import { SessionSelectionArgumentsComponent } from '../session-selection-arguments/session-selection-arguments.component';
import { WorkerExecuteArgumentsComponent } from './worker-execute-arguments.component';

const EXPECTED_DEFAULT_FIELD_LABELS: string[] = [
  'Module *',
  'Module arguments (in YAML) *',
  'Create named session',
  'Session type'
];

const MOCK_ARGUMENTS: WorkerExecuteArguments = {
  module: 'nmap',
  module_arguments: { port: 80 },
  create_named_session: 'Session 1',
  session_id: 1
};

describe('WorkerExecuteArgumentsComponent', () => {
  let component: WorkerExecuteArgumentsComponent;
  let fixture: ComponentFixture<WorkerExecuteArgumentsComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        NoopAnimationsModule,
        MatSelectModule,
        CodeEditorModule
      ],
      declarations: [WorkerExecuteArgumentsComponent, SessionSelectionArgumentsComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerExecuteArgumentsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create all available default fields', async () => {
    const fields = await loader.getAllHarnesses(MatFormFieldHarness);
    const fieldLabels = await Promise.all(fields.map(field => field.getLabel()));

    expect(fieldLabels.sort()).toEqual([...EXPECTED_DEFAULT_FIELD_LABELS].sort());
  });

  it('should fill arguments', async () => {
    component.fill(MOCK_ARGUMENTS);

    const { session_id, module_arguments, ...defaultArgs } = MOCK_ARGUMENTS;
    const stringifiedDefaultArgs = { module_arguments: stringify(module_arguments), ...defaultArgs };

    expect(component.form.value).toEqual(stringifiedDefaultArgs);
    expect(component.sessionSelectionArgs.getArguments()).toEqual({ session_id });
  });

  it('should return arguments', async () => {
    component.fill(MOCK_ARGUMENTS);
    expect(component.getArguments()).toEqual(MOCK_ARGUMENTS);
  });
});
