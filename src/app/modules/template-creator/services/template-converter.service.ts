import { Injectable } from '@angular/core';
import { stringify } from 'yaml';
import { StageNode } from '../classes/dependency-graph/node/stage-node';
import { TemplateDescription } from '../models/interfaces/template-description';
import { DependencyGraphManagerService, DepGraphRef } from './dependency-graph-manager.service';
import { TemplateCreatorStateService } from './template-creator-state.service';

@Injectable({
  providedIn: 'root'
})
export class TemplateConverterService {
  constructor(private _state: TemplateCreatorStateService, private _graphManager: DependencyGraphManagerService) {}

  /**
   * Exports template from the template creator to the YAML description.
   *
   * @returns YAML description of the template.
   */
  exportYAMLTemplate(): string {
    const templateDepGraph = this._graphManager.getCurrentGraph(DepGraphRef.TEMPLATE_CREATION).value;
    const name = this._state.templateForm.get('name').value as string;
    const owner = this._state.templateForm.get('owner').value as string;

    const template: TemplateDescription = { plan: { name, owner, stages: [] } };

    templateDepGraph.nodes.forEach((node: StageNode) => {
      const stage = node.getYaml();

      if (node.parentEdges.length > 0) {
        stage.depends_on = [];
        node.parentEdges.forEach(edge => {
          stage.depends_on.push(edge.parentNode.name);
        });
      }

      template.plan.stages.push(stage);
    });

    return stringify(template);
  }
}
