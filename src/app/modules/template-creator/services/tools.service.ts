import { Injectable } from '@angular/core';
import { DependencyGraphEditor } from '../classes/dependency-graph/dependency-graph-editor';
import { GraphViewManager } from '../classes/dependency-graph/graph-view-manager';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
  private _viewManager = new GraphViewManager();

  constructor() {}

  enableSwap(graphEditor: DependencyGraphEditor): void {
    graphEditor.toolState.flipSwapTool();
  }

  enableDelete(graphEditor: DependencyGraphEditor): void {
    graphEditor.toolState.flipDeleteTool();
  }

  enableNodeMove(graphEditor: DependencyGraphEditor): void {
    graphEditor.toolState.flipMoveNodeTool(graphEditor.depGraph.nodes);
  }

  zoomIn(graphEditor: DependencyGraphEditor): void {
    this._viewManager.zoomIn(graphEditor);
  }

  zoomOut(graphEditor: DependencyGraphEditor): void {
    this._viewManager.zoomOut(graphEditor);
  }

  fitScreen(graphEditor: DependencyGraphEditor): void {
    this._viewManager.fitScreen(graphEditor, graphEditor.depGraph);
  }
}
