import { Injectable } from '@angular/core';
import { withoutUndefinedAndNull } from 'src/app/shared/utils/without-undefined';
import { DependencyGraph } from '../classes/dependency-graph/dependency-graph';
import { StageGraphNode } from '../classes/dependency-graph/node/stage-graph-node';
import { StepArguments, StepNode } from '../classes/dependency-graph/node/step-node';
import { TriggerFactory } from '../classes/triggers/trigger-factory';
import {
  StageDescription,
  StepDescription,
  StepEdgeDescription,
  TemplateDescription
} from '../models/interfaces/template-description';

@Injectable({
  providedIn: 'root'
})
export class PlanOverviewImporterService {
  constructor() {}

  importPlan(template: TemplateDescription, depGraph: DependencyGraph) {
    const stagesWithParents: Record<string, { stage: StageGraphNode; parents: string[] }> = {};

    template.plan.stages.forEach(stageDescription => {
      const stage = this._createStageGraph(stageDescription);
      depGraph.addNode(stage);
      stagesWithParents[stageDescription.name] = {
        stage,
        parents: stageDescription.depends_on
      };
    });

    // this._createStageEdges(stagesWithParents);
  }

  private _createStageGraph(stageDescription: StageDescription): StageGraphNode {
    const depGraph = new DependencyGraph();

    const stepsWithEdges: Record<string, { step: StepNode; next: StepEdgeDescription[] }> = {};

    stageDescription.steps.forEach(stepDescription => {
      const step = this._createStep(stepDescription, depGraph);
      depGraph.addNode(step);
      stepsWithEdges[stepDescription.name] = { step, next: stepDescription.next };
    });

    const trigger = TriggerFactory.createTrigger(stageDescription.trigger_type, stageDescription.trigger_args);

    return new StageGraphNode({ name: stageDescription.name, trigger, childDepGraph: depGraph });
  }

  /**
   * Creates step from the YAML representation.
   *
   * @param stepDescription YAML description of the step.
   * @param parentDepGraph Step's parent dependency graph.
   * @returns Cryton step.
   */
  private _createStep(stepDescription: StepDescription, parentDepGraph: DependencyGraph): StepNode {
    const step = new StepNode(
      withoutUndefinedAndNull({
        name: stepDescription.name,
        type: stepDescription.step_type,
        type_arguments: stepDescription.arguments,
        output_mapping: stepDescription.output_mapping,
        output_prefix: stepDescription.output_prefix
      }) as StepArguments
    );
    step.setParentDepGraph(parentDepGraph);

    return step;
  }
}
