import Konva from 'konva';
import { Container } from 'konva/lib/Container';
import { Vector2d } from 'konva/lib/types';
import { CIRCLE_RADIUS } from 'src/app/modules/run/classes/report-step';
import { ShortStringPipe } from 'src/app/shared/pipes/short-string.pipe';
import { StrokeAnimation } from '../../animations/stroke.animation';
import { Theme } from '../../models/interfaces/theme';
import { KonvaGraphNode } from '../dependency-graph/konva-graph-node';
import { RegularTimelineNodeEventConfigurer } from './regular-timeline-node-event-configurer';
import { TimelineNode } from './timeline-node';

/**
 * Node circle radius.
 */
const NODE_RADIUS = 18;

/**
 * Padding inside the node label.
 */
const LABEL_PADDING = 5;

/**
 * Space between the label and node.
 */
const LABEL_MARGIN_BOTTOM = 5;

/**
 * Radius of the label background border.
 */
const LABEL_CORNER_RADIUS = 7;

/**
 * Font size of the name text inside the label.
 */
const NAME_FONT_SIZE = 12;

/**
 * Maximal length of name inside the label in characters.
 */
const MAX_NAME_LENGTH = 12;

/**
 * Width of stroke around the node circle.
 */
const STROKE_WIDTH = 3;

/**
 * Maximal length of primary text.
 */
const PRIMARY_LABEL_MAX_LENGTH = 10;

// Konva names (used as identifiers).
const NODE_CIRCLE_NAME = 'timelineNodeCircle';
const NODE_LABEL_NAME = 'timelineNodeLabel';
const LABEL_TAG_NAME = 'labelTag';
const LABEL_TEXT_NAME = 'labelText';

export class RegularKonvaTimelineNode implements KonvaGraphNode {
  private _konvaObject: Konva.Group;
  private _nodeCircle: Konva.Circle;
  private _primaryLabel: Konva.Label;
  private _strokeAnimation: StrokeAnimation;

  private _shortStringPipe = new ShortStringPipe();

  constructor(private _theme: Theme, x: number, primaryText: string) {
    this._konvaObject = this._createKonvaObject(this._theme, x, primaryText);
  }

  get x(): number {
    return this._konvaObject.x();
  }
  set x(value: number) {
    this._konvaObject.x(value);
  }
  get y(): number {
    return this._konvaObject.y();
  }
  set y(value: number) {
    this._konvaObject.y(value);
  }

  activateStroke(color: string, moving: boolean): void {
    if (moving) {
      this._strokeAnimation.activate(color, false);
    } else {
      this._nodeCircle.strokeEnabled(true);
      this._nodeCircle.setAttrs({
        stroke: color,
        strokeWidth: STROKE_WIDTH
      });
    }
  }

  deactivateStroke(): void {
    this._strokeAnimation.deactivate();
  }

  getWidth(): number {
    return this._nodeCircle.radius() * 2;
  }

  getHeight(): number {
    return this._nodeCircle.radius() * 2;
  }

  getKonvaObject(): Konva.Node {
    return this._konvaObject;
  }

  configureEvents(node: TimelineNode, configurer: RegularTimelineNodeEventConfigurer): void {
    configurer.configure(node, this._nodeCircle, this._primaryLabel);
  }

  destroy(): void {
    this._konvaObject.destroy();
  }

  remove(): void {
    this._konvaObject.remove();
  }

  getParentEdgePoint(): Vector2d {
    return { x: this._konvaObject.x(), y: this._konvaObject.y() };
  }

  getChildEdgePoint(): Vector2d {
    return { x: this._konvaObject.x(), y: this._konvaObject.y() };
  }

  setTheme(theme: Theme): void {
    this._nodeCircle.fill(theme.primary);

    if (this._nodeCircle.strokeWidth() > 0) {
      this._nodeCircle.stroke(theme.accent);
    }

    this._primaryLabel.getText().fill(theme.templateCreator.timemarkText);
    this._primaryLabel.getTag().fill(theme.templateCreator.labelBG);
  }

  setPrimaryText(text: string): void {
    this._primaryLabel.getText().text(this._shortStringPipe.transform(text, PRIMARY_LABEL_MAX_LENGTH));
    this._primaryLabel.x(-(this._primaryLabel.width() / 2));
  }

  setSecondaryText(text: string): void {
    throw new Error('Method not implemented.');
  }

  setDragEnabled(isEnabled: boolean): void {
    this._konvaObject.draggable(isEnabled);
  }

  copy(): KonvaGraphNode {
    throw new Error('Method not implemented.');
  }

  draw(container: Container<Konva.Node>): void {
    container.add(this.getKonvaObject());
  }

  /**
   * Adds all konva objects to the main konva group and sets configuration attributes.
   *
   * @param theme App theme.
   * @returns Konva object.
   */
  private _createKonvaObject(theme: Theme, x: number, primaryText: string): Konva.Group {
    const konvaObject = new Konva.Group({
      draggable: true,
      x
    });
    this._nodeCircle = this._createNodeCircle(theme);
    this._primaryLabel = this._createLabel(theme, primaryText);
    this._strokeAnimation = new StrokeAnimation(this._nodeCircle, this._nodeCircle);

    konvaObject.add(this._nodeCircle).add(this._primaryLabel);

    return konvaObject;
  }

  /**
   * Creates the konva node circle.
   *
   * @returns Konva circle.
   */
  private _createNodeCircle(theme: Theme): Konva.Circle {
    const circle = new Konva.Circle({
      name: NODE_CIRCLE_NAME,
      radius: NODE_RADIUS,
      strokeWidth: 0
    });

    circle.fill(theme.primary);

    circle.on('click', e => {
      e.cancelBubble = true;
    });

    return circle;
  }

  /**
   * Creates the label for the timeline node.
   *
   * @param theme App theme.
   * @param text Text content.
   * @returns Konva label.
   */
  private _createLabel(theme: Theme, text: string): Konva.Label {
    const label = new Konva.Label({ listening: false, name: NODE_LABEL_NAME });
    const tag = this._createTag(theme);
    const konvaText = this._createText(theme, text);

    label.add(tag).add(konvaText);
    label.x(-label.width() / 2);
    label.y(-CIRCLE_RADIUS - label.height() - 2 * LABEL_PADDING - LABEL_MARGIN_BOTTOM);

    return label;
  }

  /**
   * Creates the background rect of the name tag.
   *
   * @param theme App theme.
   * @returns Konva rect forming the name tag background.
   */
  private _createTag(theme: Theme): Konva.Tag {
    const nameTag = new Konva.Tag({
      cornerRadius: LABEL_CORNER_RADIUS,
      listening: false,
      name: LABEL_TAG_NAME
    });

    nameTag.fill(theme.templateCreator.labelBG);

    return nameTag;
  }

  /**
   * Creates the name text inside the node name tag.
   *
   * @param theme App theme.
   * @param text Text content.
   * @returns Konva text representing the node name text.
   */
  private _createText(theme: Theme, text: string): Konva.Text {
    const nameText = new Konva.Text({
      text: this._shortStringPipe.transform(text, MAX_NAME_LENGTH),
      fontFamily: 'roboto',
      fontSize: NAME_FONT_SIZE,
      y: -NODE_RADIUS - NAME_FONT_SIZE - LABEL_MARGIN_BOTTOM,
      padding: 5,
      listening: false,
      name: LABEL_TEXT_NAME
    });

    nameText.fill(theme.templateCreator.timemarkText);

    nameText.x(-(nameText.width() / 2));
    return nameText;
  }
}
