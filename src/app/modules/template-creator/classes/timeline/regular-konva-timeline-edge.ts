import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { StrokeAnimation } from '../../animations/stroke.animation';
import { Theme } from '../../models/interfaces/theme';
import { KonvaGraphEdge } from '../dependency-graph/edge/konva-graph-edge';
import { EventConfigurer } from '../dependency-graph/event-configurer';
import { KonvaGraphNode } from '../dependency-graph/konva-graph-node';
import { Vector } from '../utils/vector';

const EDGE_ARROW_NAME = 'timelineEdgeArrow';

export class RegularKonvaTimelineEdge implements KonvaGraphEdge {
  private _konvaObject: Konva.Arrow;
  private _strokeAnimation: StrokeAnimation;

  private _startTrackedNode: KonvaGraphNode;
  private _endTrackedNode: KonvaGraphNode;

  constructor(private _theme: Theme, initialPoint: Vector2d) {
    this._createKonvaObject(_theme, initialPoint);
  }

  public get start(): Vector2d {
    const points = this._konvaObject.points();
    return { x: points[0], y: points[1] };
  }

  public set start(value: Vector2d) {
    const points = this._konvaObject.points();
    points[0] = value.x;
    points[1] = value.y;
    this._konvaObject.points(points);
  }

  public get end(): Vector2d {
    const points = this._konvaObject.points();
    return { x: points[2], y: points[3] };
  }

  public set end(value: Vector2d) {
    const points = this._konvaObject.points();
    points[2] = value.x;
    points[3] = value.y;
    this._konvaObject.points(points);
  }

  trackWithStart(node: KonvaGraphNode): void {
    if (this._startTrackedNode) {
      this._startTrackedNode.getKonvaObject().off('dragmove');
    }

    this._startTrackedNode = node;
    this._moveToParentNode();

    node.getKonvaObject().on('dragmove', () => {
      this._moveToParentNode();
    });
  }

  trackWithEnd(node: KonvaGraphNode): void {
    if (this._endTrackedNode) {
      this._endTrackedNode.getKonvaObject().off('dragmove');
    }

    this._endTrackedNode = node;
    this._moveToChildNode();

    node.getKonvaObject().on('dragmove', () => {
      this._moveToChildNode();
    });
  }

  refreshTracking(): void {
    this._moveToParentNode();
    this._moveToChildNode();
  }

  getKonvaObject(): Konva.Node {
    return this._konvaObject;
  }

  configureEvents(configurer: EventConfigurer): void {
    throw new Error('Method not implemented.');
  }

  destroy(): void {
    this._konvaObject.destroy();
  }

  setTheme(theme: Theme): void {
    this._theme = theme;

    const edgePoints = this._konvaObject.points();

    if (edgePoints[2] - edgePoints[0] >= 0) {
      this._konvaObject.stroke(theme.templateCreator.timelineEdge);
      this._konvaObject.fill(theme.templateCreator.timelineEdge);
    }
  }

  activateStroke(color: string, moving: boolean): void {
    throw new Error('Method not implemented.');
  }

  deactivateStroke(): void {
    throw new Error('Method not implemented.');
  }

  draw(container: Konva.Container): void {
    container.add(this._konvaObject);
  }

  private _moveToParentNode(): void {
    this.start = this._startTrackedNode.getChildEdgePoint();

    if (this._endTrackedNode) {
      // In this case we need to move to child node as well
      // to update the tip position.
      this._moveToChildNode();
      this._handleEdgeColoring(this._startTrackedNode.x, this._endTrackedNode.x);
    }
  }

  private _moveToChildNode(): void {
    // If we track with start of the edge as well, we can calculate
    // the intersection of the end node and the edge to move the edge tip
    // right at the intersection. This way we can see the whole edge tip.
    if (this._startTrackedNode) {
      const childIntersect = this._findIntersect(
        new Vector(this._endTrackedNode.x, this._endTrackedNode.y),
        this._endTrackedNode.getWidth() / 2,
        new Vector(this._startTrackedNode.x, this._startTrackedNode.y)
      );
      this.end = { x: childIntersect.x, y: childIntersect.y };
      this._handleEdgeColoring(this._startTrackedNode.x, this._endTrackedNode.x);
    } else {
      this.end = this._endTrackedNode.getParentEdgePoint();
    }
  }

  private _handleEdgeColoring(parentX: number, childX: number): void {
    if (parentX > childX) {
      this._konvaObject.fill('red');
      this._konvaObject.stroke('red');
    } else {
      this._konvaObject.fill(this._theme.templateCreator.timelineEdge);
      this._konvaObject.stroke(this._theme.templateCreator.timelineEdge);
    }
  }

  /**
   * Creates konva object and sets up event listener functions.
   */
  private _createKonvaObject(theme: Theme, { x, y }: Vector2d): void {
    this._konvaObject = new Konva.Arrow({
      points: [x, y, x, y],
      strokeWidth: 2,
      pointerWidth: 4,
      pointerLength: 6,
      hitStrokeWidth: 10,
      shadowForStrokeEnabled: false,
      name: EDGE_ARROW_NAME
    });

    this.setTheme(theme);
  }

  /**
   * Finds the intersection of a line starting at the origin and ending at the endPoint and a
   * circle with a center at the origin with a given radius.
   *
   * @param origin Origin point of the line and the center of the circle.
   * @param radius Circle radius.
   * @param endPoint End point of the line.
   * @returns Intersection point.
   */
  private _findIntersect(origin: Vector, radius: number, endPoint: Vector): Vector {
    let v = endPoint.subtract(origin);
    const lineLength = v.length();
    if (lineLength === 0) {
      return origin;
    }
    v = v.normalize();
    return origin.add(v.multiplyScalar(radius));
  }
}
