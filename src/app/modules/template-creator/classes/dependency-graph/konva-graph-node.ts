import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { Theme } from '../../models/interfaces/theme';
import { EventConfigurer } from './event-configurer';
import { GraphNode } from './node/graph-node';

export interface KonvaGraphNode {
  x: number;

  y: number;

  getWidth(): number;

  getHeight(): number;

  getKonvaObject(): Konva.Node;

  configureEvents(node: GraphNode, configurer: EventConfigurer): void;

  destroy(): void;

  remove(): void;

  getParentEdgePoint(): Vector2d;

  getChildEdgePoint(): Vector2d;

  setTheme(theme: Theme): void;

  setPrimaryText(text: string): void;

  setSecondaryText(text: string): void;

  activateStroke(color: string, moving: boolean): void;

  deactivateStroke(): void;

  setDragEnabled(isEnabled: boolean): void;

  copy(): KonvaGraphNode;

  draw(container: Konva.Container): void;
}
