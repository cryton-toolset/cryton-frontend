import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { Theme } from '../../../models/interfaces/theme';
import { EventConfigurer } from '../event-configurer';
import { KonvaGraphNode } from '../konva-graph-node';

export interface KonvaGraphEdge {
  start: Vector2d;
  end: Vector2d;

  /**
   * Tracks position of node on every dragmove event.
   * Adjusts starting point of the edge according to movement of the node.
   *
   * @param node Konva graph node.
   */
  trackWithStart(node: KonvaGraphNode | null): void;

  /**
   * Tracks position of node on every dragmove event.
   * Adjusts ending point of the edge according to movement of the node.
   *
   * @param node Konva graph node.
   */
  trackWithEnd(node: KonvaGraphNode | null): void;

  /**
   * Manually refreshes node tracking: used when nodes get moved by other means than dragging.
   */
  refreshTracking(): void;

  getKonvaObject(): Konva.Node;

  configureEvents(configurer: EventConfigurer): void;

  destroy(): void;

  setTheme(theme: Theme): void;

  activateStroke(color: string, moving: boolean): void;

  deactivateStroke(): void;

  draw(container: Konva.Container): void;
}
