import { StageNode } from '../node/stage-node';
import { GraphEdge } from './graph-edge';

export class StageEdge extends GraphEdge {
  parentNode: StageNode;
  childNode: StageNode;

  constructor(parentNode: StageNode) {
    super(parentNode);
  }

  /**
   * Connects edge to the child node.
   * If child node has trigger type of delta, looks for all parent deltas
   * to create transitive timeline edges between delta stages.
   *
   * @param childNode Child node to connect to.
   */
  connect(childNode: StageNode): void {
    try {
      super.connect(childNode);
      this.isCorrectStageEdge();
    } catch (error) {
      throw error;
    }
  }

  /**
   * Checks if edge from parent node to child node is a correct edge.
   * There must be no cycles.
   * There can't already be the same edge.
   */
  isCorrectStageEdge(): void {
    const childStart = this.childNode.trigger.getStartTime();
    const parentStart = this.parentNode.trigger.getStartTime();

    if (childStart && parentStart && parentStart >= childStart) {
      this.destroy();
      throw new Error(`Child stage trigger must start later or at the same time as every parent stage trigger.`);
    }
  }
}
