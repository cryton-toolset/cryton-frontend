import Konva from 'konva';
import { KonvaGraphNode } from '../konva-graph-node';
import { GraphNode } from '../node/graph-node';
import { KonvaGraphEdge } from './konva-graph-edge';

export abstract class GraphEdge {
  parentNode: GraphNode;
  childNode: GraphNode;
  konvaGraphEdge: KonvaGraphEdge;

  constructor(parentNode: GraphNode) {
    this.parentNode = parentNode;
    this.parentNode.addChildEdge(this);
  }

  /**
   * Checks if edge from parent node to child node is a correct edge.
   * There must be no cycles.
   * There can't already be the same edge.
   */
  isCorrectEdge(): void {
    for (const childEdge of this.parentNode.childEdges) {
      if (childEdge !== this && childEdge.childNode === this.childNode) {
        throw new Error('Edge already exists.');
      }
    }
    if (this.doesCreateCycle()) {
      throw new Error('Edge creates a cycle.');
    }
  }

  /**
   * Checks if edge creates a cycle.
   *
   * @param startEdge Edge where call stack started, should be left empty.
   * @returns True if edge creates a cycle.
   */
  doesCreateCycle(startEdge: GraphEdge = this): boolean {
    return this.childNode.childEdges.some(childEdge => childEdge === startEdge || childEdge.doesCreateCycle(startEdge));
  }

  /**
   * Connects edge to the child node.
   *
   * @param childNode Child node to connect to.
   */
  connect(childNode: GraphNode): void {
    this.childNode = childNode;
    this.childNode.addParentEdge(this);

    try {
      this.isCorrectEdge();
    } catch (error) {
      this.destroy();
      throw error;
    }

    this._initNodeTracking(this.parentNode.konvaGraphNode, this.childNode.konvaGraphNode);
  }

  /**
   * Destroys edge.
   */
  destroy(): void {
    this.parentNode.removeChildEdge(this);
    this.childNode?.removeParentEdge(this);
    this.konvaGraphEdge.destroy();
    this.konvaGraphEdge = null;
  }

  addTo(container: Konva.Container): void {
    container.add(this.konvaGraphEdge.getKonvaObject());
  }

  private _initNodeTracking(parent: KonvaGraphNode, child: KonvaGraphNode): void {
    this.konvaGraphEdge.trackWithStart(parent);
    this.konvaGraphEdge.trackWithEnd(child);
  }
}
