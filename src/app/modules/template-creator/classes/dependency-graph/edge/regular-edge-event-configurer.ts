import Konva from 'konva';
import { EventConfigurer } from '../event-configurer';

export interface RegularEdgeEventConfigurer extends EventConfigurer {
  configure(arrow: Konva.Arrow): void;

  removeOwnListeners(arrow: Konva.Arrow): void;

  removeAllListeners(arrow: Konva.Arrow): void;
}
