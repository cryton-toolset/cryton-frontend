import { GraphNode } from './node/graph-node';

export class ToolState {
  isSwapEnabled = false;
  isDeleteEnabled = false;
  isMoveNodeEnabled = false;

  private _lastlySetNodesForMoveNode: GraphNode[] = [];

  constructor() {}

  flipSwapTool(): boolean {
    this.isSwapEnabled = this._toolClick(this.isSwapEnabled);
    return this.isSwapEnabled;
  }

  flipDeleteTool(): boolean {
    this.isDeleteEnabled = this._toolClick(this.isDeleteEnabled);
    return this.isDeleteEnabled;
  }

  flipMoveNodeTool(nodes: GraphNode[]): boolean {
    this.isMoveNodeEnabled = this._toolClick(this.isMoveNodeEnabled);
    this._lastlySetNodesForMoveNode = nodes;

    nodes.forEach(node => node.konvaGraphNode.setDragEnabled(this.isMoveNodeEnabled));

    return this.isMoveNodeEnabled;
  }

  reset(): void {
    this.isSwapEnabled = false;
    this.isDeleteEnabled = false;
    this.isMoveNodeEnabled = false;
    this._lastlySetNodesForMoveNode.forEach(node => node.konvaGraphNode.setDragEnabled(this.isMoveNodeEnabled));
  }

  /**
   * Disables all tools and returns inverse value of isToolEnabled.
   *
   * @param isToolEnabled Value of is<ToolName>Enabled of clicked tool.
   */
  private _toolClick(isToolEnabled: boolean): boolean {
    // Swap tool
    this.isSwapEnabled = false;

    // Delete tool
    this.isDeleteEnabled = false;

    // Move node tool
    this.isMoveNodeEnabled = false;

    return !isToolEnabled;
  }
}
