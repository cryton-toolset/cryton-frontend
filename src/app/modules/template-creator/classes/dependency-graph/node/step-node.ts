import { StepNodeType } from 'src/app/modules/template-creator/models/enums/step-node-type.enum';
import { StepArgumentsType } from 'src/app/modules/template-creator/models/types/step-node.type';
import { OutputMapping } from '../../../models/interfaces/output-mapping';
import { StepDescription } from '../../../models/interfaces/template-description';
import { GraphNode } from './graph-node';

export interface StepArguments {
  name: string;
  type: StepNodeType;
  type_arguments: StepArgumentsType;
  output_prefix?: string;
  output_mapping?: OutputMapping[];
}

export class StepNode extends GraphNode {
  constructor(public args: StepArguments) {
    super(args.name, args.type);
  }

  /**
   * Edits step args.
   *
   * @param args New step args.
   */
  edit(args: StepArguments): void {
    this.name = args.name;
    this.note = args.type;
    this.args = args;
  }

  copy(): StepNode {
    const argsCopy = Object.assign({}, this.args);
    const nodeCopy = new StepNode(argsCopy);

    if (this.konvaGraphNode) {
      nodeCopy.konvaGraphNode = this.konvaGraphNode.copy();
    }

    return nodeCopy;
  }

  getYaml(): StepDescription {
    return {
      name: this.name,
      step_type: this.args.type,
      arguments: this.args.type_arguments,
      output_mapping: this.args.output_mapping,
      output_prefix: this.args.output_prefix
    };
  }
}
