import Konva from 'konva';
import { Cursor } from '../cursor-state';
import { DependencyGraphOverview } from '../dependency-graph-overview';
import { CompositeNodeEventConfigurer } from './composite-node-event-configurer';
import { GraphNode } from './graph-node';
import { OverviewRegularNodeConfigurer } from './overview-regular-node-configurer';
import { StageNode } from './stage-node';

export class OverviewCompositeNodeConfigurer implements CompositeNodeEventConfigurer {
  private _regularNodeConfigurer: OverviewRegularNodeConfigurer;

  constructor(private _wrapper: DependencyGraphOverview) {
    this._regularNodeConfigurer = new OverviewRegularNodeConfigurer(this._wrapper);
  }

  configure(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void {
    this.removeOwnListeners(node, primaryLabel, secondaryLabel, borderRect, topLine, bottomLine);

    primaryLabel.on('mouseenter.overview-composite-node', () => {
      this._wrapper.cursorState.setCursor(Cursor.POINTER);
    });

    primaryLabel.on('mouseleave.overview-composite-node', () => {
      this._wrapper.cursorState.unsetCursor(Cursor.POINTER);
    });

    primaryLabel.on('click.overview-composite-node', ({ evt }) => {
      this._wrapper.displayTooltip(node as StageNode, { x: evt.offsetX, y: evt.offsetY });
    });

    (node as StageNode).childDepGraph.nodes.forEach(node => {
      node.configureEvents(this._regularNodeConfigurer);
    });
  }

  removeOwnListeners(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void {
    primaryLabel.off('mouseenter.overview-composite-node');
    primaryLabel.off('mouseleave.overview-composite-node');
    primaryLabel.off('click.overview-composite-node');
  }

  removeAllListeners(
    node: GraphNode,
    primaryLabel: Konva.Label,
    secondaryLabel: Konva.Label,
    borderRect: Konva.Rect,
    topLine: Konva.Line,
    bottomLine: Konva.Line
  ): void {
    node.konvaGraphNode.getKonvaObject().off();
    this._removeLabelListeners(primaryLabel);
    this._removeLabelListeners(secondaryLabel);
    borderRect.off();
    topLine.off();
    bottomLine.off();
  }

  private _removeLabelListeners(label: Konva.Label): void {
    label.getTag().off();
    label.getText().off();
    label.off();
  }
}
