import Konva from 'konva';
import { RippleAnimation } from 'src/app/modules/template-creator/animations/ripple.animation';
import { Cursor, CursorState } from '../../cursor-state';

/**
 * Click and ripple radius of button (not the actual cogwheel).
 */
export const BTN_RADIUS = 16;

/**
 * SVG path should have a size of 24x24 pixels.
 */
export const BTN_SIZE = 24;

export const BTN_NAME = 'graphNodeBtn';

export abstract class GraphNodeButton {
  private _konvaObject: Konva.Group;

  private _rippleAnimation: RippleAnimation;
  private _rippleCircle: Konva.Circle;

  constructor(private _cursorState: CursorState, private _path: string, private _clickCallback: () => void) {
    this._konvaObject = new Konva.Group({ name: BTN_NAME });

    this._rippleCircle = new Konva.Circle({
      radius: 0,
      fill: '#49565e',
      x: BTN_SIZE / 2,
      y: BTN_SIZE / 2
    });

    const buttonIcon = this._createButtonIcon();

    this._konvaObject.add(this._rippleCircle);
    this._konvaObject.add(buttonIcon);

    this._initEventListeners();
    this._rippleAnimation = new RippleAnimation(this._rippleCircle);
  }

  getKonvaObject(): Konva.Group {
    return this._konvaObject;
  }

  changeCursorStateObject(cursorState: CursorState): void {
    this._cursorState = cursorState;
    this._konvaObject.off('mouseenter');
    this._konvaObject.off('mouseleave');
    this._initHoverEvents();
  }

  /**
   * Creates a button icon as Konva image from svg.
   */
  private _createButtonIcon(): Konva.Path {
    const icon = new Konva.Path({
      data: this._path,
      fill: 'white',
      hitFunc: (context: Konva.Context, shape: Konva.Shape) => {
        context.beginPath();
        context.arc(BTN_SIZE / 2, BTN_SIZE / 2, BTN_RADIUS, 0, 2 * Math.PI, true);
        context.closePath();
        context.fillStrokeShape(shape);
      }
    });

    return icon;
  }

  /**
   * Initializes hover events.
   */
  private _initHoverEvents(): void {
    this._konvaObject.on('mouseenter', () => {
      this._cursorState.setCursor(Cursor.POINTER);
    });
    this._konvaObject.on('mouseleave', () => {
      this._cursorState.unsetCursor(Cursor.POINTER);
    });
  }

  /**
   * Initializes ripple event.
   */
  private _initRippleEvents(): void {
    this._konvaObject.on('mousedown', () => this._rippleAnimation.animate(BTN_RADIUS, 1));
    this._konvaObject.on('mouseup mouseleave', () => this._rippleAnimation.unanimate());
  }

  private _initClickEvent(): void {
    this._konvaObject.on('click', this._clickCallback);
  }

  /**
   * Initializes all event listeners.
   */
  private _initEventListeners() {
    this._initHoverEvents();
    this._initRippleEvents();
    this._initClickEvent();
  }
}
