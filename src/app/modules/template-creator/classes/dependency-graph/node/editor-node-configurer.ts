import Konva from 'konva';
import { Rect } from 'konva/lib/shapes/Rect';
import { Text } from 'konva/lib/shapes/Text';
import { Cursor } from '../cursor-state';
import { DependencyGraphEditor } from '../dependency-graph-editor';
import { KonvaGraphNode } from '../konva-graph-node';
import { NodeConnector } from '../node-connector';
import { GraphNode } from './graph-node';
import { RegularNodeEventConfigurer } from './regular-node-event-configurer';

export class EditorNodeEventConfigurer implements RegularNodeEventConfigurer {
  constructor(private _wrapper: DependencyGraphEditor) {}

  configure(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void {
    const konvaObject = node.konvaGraphNode.getKonvaObject();

    this.removeOwnListeners(node, primaryText, secondaryText, rectangle, connector);
    this._initWholeBodyEvents(this._wrapper, node.konvaGraphNode, node, konvaObject);
    this._initNodeRectEvents(this._wrapper, rectangle, node.konvaGraphNode, node);
    this._initConnectorEvents(this._wrapper, node, connector);

    if (this._wrapper.toolState.isMoveNodeEnabled) {
      konvaObject.draggable(true);
    }
  }

  removeOwnListeners(
    node: GraphNode,
    primaryText: Text,
    secondaryText: Text,
    rectangle: Rect,
    connector: NodeConnector
  ): void {
    const konvaObject = node.konvaGraphNode.getKonvaObject();
    const connectorKonvaObject = connector.getKonvaObject();

    konvaObject.off('click.editor-node');
    konvaObject.off('mouseleave.editor-node');
    konvaObject.off('mouseenter.editor-node');
    rectangle.off('click.editor-node');
    connectorKonvaObject.off('mouseenter.editor-node');
    connectorKonvaObject.off('mouseleave.editor-node');
    connectorKonvaObject.off('click.editor-node');
    connectorKonvaObject.off('mousedown.editor-node');
    connectorKonvaObject.off('mouseup.editor-node');
  }

  removeAllListeners(
    node: GraphNode,
    primaryText: Text,
    secondaryText: Text,
    rectangle: Rect,
    connector: NodeConnector
  ): void {
    node.konvaGraphNode.getKonvaObject().off();
    primaryText.off();
    secondaryText.off();
    rectangle.off();
    connector.getKonvaObject().off();
  }

  private _initWholeBodyEvents(
    graphEditor: DependencyGraphEditor,
    konvaGraphNode: KonvaGraphNode,
    graphNode: GraphNode,
    konvaObject: Konva.Node
  ): void {
    konvaObject.on('click.editor-node', () => {
      if (graphEditor.draggedEdge) {
        try {
          graphEditor.connectDraggedEdge(graphNode);
        } catch (err) {
          graphEditor.draggedEdge = null;
        }
      }
    });
    konvaObject.on('mouseleave.editor-node', () => {
      if (graphEditor.toolState.isSwapEnabled || graphEditor.toolState.isDeleteEnabled) {
        graphEditor.cursorState.unsetCursor(Cursor.POINTER);
      }
      if (graphEditor.toolState.isMoveNodeEnabled) {
        graphEditor.cursorState.unsetCursor(Cursor.GRAB);
      }
    });

    konvaObject.on('mouseenter.editor-node', () => {
      if (graphEditor.toolState.isSwapEnabled) {
        konvaGraphNode.activateStroke(graphEditor.theme.primary, true);
        graphEditor.cursorState.setCursor(Cursor.POINTER);
      } else if (graphEditor.toolState.isDeleteEnabled) {
        konvaGraphNode.activateStroke(graphEditor.theme.primary, true);
        graphEditor.cursorState.setCursor(Cursor.POINTER);
      } else if (graphEditor.toolState.isMoveNodeEnabled) {
        graphEditor.cursorState.setCursor(Cursor.GRAB);
      }
    });
  }

  private _initNodeRectEvents(
    graphEditor: DependencyGraphEditor,
    nodeRect: Konva.Rect,
    konvaGraphNode: KonvaGraphNode,
    graphNode: GraphNode
  ): void {
    nodeRect.on('click.editor-node', () => {
      if (graphEditor.toolState.isSwapEnabled) {
        konvaGraphNode.deactivateStroke();
        graphEditor.cursorState.resetCursor();
        graphEditor.swapNode(graphNode);
      } else if (graphEditor.toolState.isDeleteEnabled) {
        graphEditor.depGraph.clearEditNode();
        graphEditor.cursorState.resetCursor();
        graphEditor.deleteNode(graphNode);
      }
    });
  }

  private _initConnectorEvents(
    graphEditor: DependencyGraphEditor,
    graphNode: GraphNode,
    connector: NodeConnector
  ): void {
    const connectorKonvaObj = connector.getKonvaObject();

    connectorKonvaObj.on('mouseenter.editor-node', () => {
      graphEditor.cursorState.setCursor(Cursor.POINTER);
    });

    connectorKonvaObj.on('mouseleave.editor-node', () => {
      graphEditor.cursorState.unsetCursor(Cursor.POINTER);
      connector.unanimateRipple();
    });

    connectorKonvaObj.on('click.editor-node', event => {
      event.cancelBubble = true;
      this._handleConnectorClick(graphEditor, graphNode);
    });
    connectorKonvaObj.on('mousedown.editor-node', () => connector.animateRipple(15, 0.2));
    connectorKonvaObj.on('mouseup.editor-node', () => connector.unanimateRipple());
  }

  private _handleConnectorClick = (graphEditor: DependencyGraphEditor, node: GraphNode): void => {
    if (graphEditor.draggedEdge || graphEditor.toolState.isDeleteEnabled || graphEditor.toolState.isSwapEnabled) {
      return;
    }
    graphEditor.clickedNode = node;
    graphEditor.createDraggedEdge(node);
  };
}
