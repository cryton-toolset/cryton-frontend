import Konva from 'konva';
import { Vector2d } from 'konva/lib/types';
import { ShortStringPipe } from 'src/app/shared/pipes/short-string.pipe';
import { StrokeAnimation } from '../../../animations/stroke.animation';
import { Theme } from '../../../models/interfaces/theme';
import { KonvaGraphNode } from '../konva-graph-node';
import { NodeConnector } from '../node-connector';
import { BTN_SIZE, GraphNodeButton } from './buttons/graph-node-button';
import { GraphNode } from './graph-node';
import { RegularNodeEventConfigurer } from './regular-node-event-configurer';

const GRAPH_NODE_NAME = 'graphNode';
const GRAPH_NODE_RECT_NAME = 'graphNodeRect';
const GRAPH_NODE_TEXT_NAME = 'graphNodeName';

const NODE_WIDTH = 170;
const NODE_HEIGHT = 60;
const NODE_BORDER_RADIUS = 10;
const NODE_PADDING = 10;

const MAX_PRIMARY_TEXT_LENGTH = 17;
const MAX_SECONDARY_TEXT_LENGTH = 22;
const NAME_TEXT_SIZE = 14;
const NOTE_TEXT_SIZE = 11;
const TEXT_GAP = 5;

export class RegularKonvaGraphNode implements KonvaGraphNode {
  private _konvaObject: Konva.Group;

  private _primaryText: Konva.Text;
  private _secondaryText: Konva.Text;
  private _rectangle: Konva.Rect;
  private _connector: NodeConnector;
  private _strokeAnimation: StrokeAnimation;

  constructor(private _theme: Theme, primaryText: string, secondaryText: string, private _button?: GraphNodeButton) {
    this._konvaObject = this._createKonvaObject(_theme, primaryText, secondaryText, _button);
  }

  get x(): number {
    return this._konvaObject.x();
  }
  set x(value: number) {
    this._konvaObject.x(value);
  }

  get y(): number {
    return this._konvaObject.y();
  }
  set y(value: number) {
    this._konvaObject.y(value);
  }

  getWidth(): number {
    return NODE_WIDTH;
  }

  getHeight(): number {
    return NODE_HEIGHT;
  }

  getParentEdgePoint(): Vector2d {
    return { x: this.x + this.getWidth() / 2, y: this.y };
  }

  getChildEdgePoint(): Vector2d {
    return { x: this.x + this.getWidth() / 2, y: this.y + this.getHeight() };
  }

  destroy(): void {
    this._konvaObject.destroy();
  }

  remove(): void {
    this._konvaObject.remove();
  }

  configureEvents(node: GraphNode, configurer: RegularNodeEventConfigurer): void {
    configurer.configure(node, this._primaryText, this._secondaryText, this._rectangle, this._connector);
  }

  getKonvaObject(): Konva.Group {
    return this._konvaObject;
  }

  setTheme(theme: Theme): void {
    this._rectangle.fill(theme.templateCreator.graphNodeRect);
    this._connector.changeTheme(theme);
  }

  setButton(button: GraphNodeButton): void {
    if (this._button) {
      this._button.getKonvaObject().destroy();
      this._button = button;
    }

    const btnKonvaObject = button.getKonvaObject();
    this._konvaObject.add(button.getKonvaObject());
    this._positionButton(button);
    btnKonvaObject.moveToTop();
  }

  activateStroke(color: string, moving: boolean): void {
    if (moving) {
      this._strokeAnimation.activate(color);
    } else {
      this._rectangle.strokeEnabled(true);
      this._rectangle.stroke(color);
      this._rectangle.strokeWidth(3);
      this._rectangle.lineCap('round');
    }
  }

  deactivateStroke(): void {
    this._strokeAnimation.deactivate();
  }

  setPrimaryText(text: string) {
    this._primaryText?.text(new ShortStringPipe().transform(text, MAX_PRIMARY_TEXT_LENGTH));
  }

  setSecondaryText(text: string) {
    this._secondaryText?.text(new ShortStringPipe().transform(text, MAX_SECONDARY_TEXT_LENGTH));
  }

  setDragEnabled(isEnabled: boolean): void {
    this._konvaObject.draggable(isEnabled);
  }

  draw(container: Konva.Container): void {
    container.add(this.getKonvaObject());
  }

  copy(): RegularKonvaGraphNode {
    const copyNode = new RegularKonvaGraphNode(
      this._theme,
      this._primaryText.text(),
      this._secondaryText.text(),
      this._button
    );

    copyNode.x = this.x;
    copyNode.y = this.y;

    return copyNode;
  }

  private _positionButton(button: GraphNodeButton): void {
    const buttonKonvaObject = button.getKonvaObject();

    buttonKonvaObject.x(NODE_WIDTH - BTN_SIZE - NODE_PADDING);
    buttonKonvaObject.y(NODE_HEIGHT / 2 - BTN_SIZE / 2);
  }

  /**
   * Initializes node konva object.
   */
  private _createKonvaObject(
    theme: Theme,
    primaryText: string,
    secondaryText: string,
    button?: GraphNodeButton
  ): Konva.Group {
    const konvaObject = new Konva.Group({ name: GRAPH_NODE_NAME });
    const connector = this._createNodeConnector(theme);

    this._rectangle = this._createNodeRect(theme);
    this._strokeAnimation = new StrokeAnimation(this._rectangle, konvaObject);

    konvaObject.add(this._rectangle);
    konvaObject.add(connector);
    this._renderText(konvaObject, primaryText, secondaryText);

    if (button) {
      konvaObject.add(button.getKonvaObject());
      this._positionButton(button);
    }

    return konvaObject;
  }

  /**
   * Creates node rectangle.
   *
   * @returns Node rectangle
   */
  private _createNodeRect(theme: Theme): Konva.Rect {
    const rect = new Konva.Rect({
      width: NODE_WIDTH,
      height: NODE_HEIGHT,
      name: GRAPH_NODE_RECT_NAME,
      cornerRadius: NODE_BORDER_RADIUS
    });

    rect.fill(theme.templateCreator.graphNodeRect);

    return rect;
  }

  /**
   * Creates a red connector circle.
   */
  private _createNodeConnector(theme: Theme): Konva.Group {
    this._connector = new NodeConnector(NODE_WIDTH, NODE_HEIGHT);
    this._connector.changeTheme(theme);

    return this._connector.getKonvaObject();
  }

  private _renderText(konvaObject: Konva.Group, primaryText: string, secondaryText?: string): void {
    this._primaryText = this._createPrimaryText(primaryText);

    this._secondaryText = this._createNoteText(secondaryText);
    const totalTextHeight = NAME_TEXT_SIZE + TEXT_GAP + NOTE_TEXT_SIZE;
    const leftoverHeight = NODE_HEIGHT - totalTextHeight;

    this._primaryText.y(leftoverHeight / 2);
    this._secondaryText.y(NAME_TEXT_SIZE + TEXT_GAP + leftoverHeight / 2);
    this._secondaryText.x(NODE_PADDING);

    konvaObject.add(this._secondaryText);

    this._primaryText.x(NODE_PADDING);
    konvaObject.add(this._primaryText);
  }

  /**
   * Creates Konva text object.
   *
   * @param text Text content.
   * @param maxLength Maximal length of text, anythin longer will be hidden behind elipsis.
   * @returns Konva text object.
   */
  private _createText(text: string, maxLength: number): Konva.Text {
    return new Konva.Text({
      text: new ShortStringPipe().transform(text, maxLength),
      fontFamily: 'Roboto',
      listening: false,
      name: GRAPH_NODE_TEXT_NAME
    });
  }

  private _createPrimaryText(name: string): Konva.Text {
    const nameText = this._createText(name, MAX_PRIMARY_TEXT_LENGTH);

    nameText.setAttrs({
      fontSize: NAME_TEXT_SIZE,
      fontStyle: '500',
      fill: 'white'
    });

    return nameText;
  }

  private _createNoteText(note: string): Konva.Text {
    const nameText = this._createText(note, MAX_SECONDARY_TEXT_LENGTH);

    nameText.setAttrs({
      fontSize: NOTE_TEXT_SIZE,
      fontStyle: '400',
      fill: '#bababa'
    });

    return nameText;
  }
}
