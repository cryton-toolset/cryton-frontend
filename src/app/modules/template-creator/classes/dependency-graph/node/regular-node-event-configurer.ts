import Konva from 'konva';
import { EventConfigurer } from '../event-configurer';
import { NodeConnector } from '../node-connector';
import { GraphNode } from './graph-node';

export interface RegularNodeEventConfigurer extends EventConfigurer {
  configure(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void;

  removeOwnListeners(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void;

  removeAllListeners(
    node: GraphNode,
    primaryText: Konva.Text,
    secondaryText: Konva.Text,
    rectangle: Konva.Rect,
    connector: NodeConnector
  ): void;
}
