import Konva from 'konva';
import { GraphEdge } from '../edge/graph-edge';
import { EventConfigurer } from '../event-configurer';
import { KonvaGraphNode } from '../konva-graph-node';

export class GraphNode {
  childEdges: GraphEdge[] = [];
  parentEdges: GraphEdge[] = [];
  konvaGraphNode: KonvaGraphNode;

  constructor(private _name: string, private _note: string) {}

  get name(): string {
    return this._name;
  }
  set name(value: string) {
    this._name = value;
    this.konvaGraphNode?.setPrimaryText(value);
  }

  get note(): string {
    return this._note;
  }
  set note(value: string) {
    this._note = value;
    this.konvaGraphNode?.setSecondaryText(value);
  }

  addParentEdge(edge: GraphEdge): void {
    this.parentEdges.push(edge);
  }

  addChildEdge(edge: GraphEdge): void {
    this.childEdges.push(edge);
  }

  removeParentEdge(edge: GraphEdge): void {
    this._removeEdge(this.parentEdges, edge);
  }

  removeChildEdge(edge: GraphEdge): void {
    this._removeEdge(this.childEdges, edge);
  }

  /**
   * Destroys all edges.
   */
  destroyEdges(): void {
    [...this.parentEdges].forEach(edge => edge.destroy());
    [...this.childEdges].forEach(edge => edge.destroy());
    this.parentEdges = [];
    this.childEdges = [];
  }

  /**
   * Adds konva object to the specified container.
   *
   * @param container Konva container.
   */
  addTo(container: Konva.Container): void {
    container.add(this.konvaGraphNode.getKonvaObject());
  }

  /**
   * Destroys node.
   */
  destroy(): void {
    this.destroyEdges();
    this.konvaGraphNode.destroy();
    this.konvaGraphNode = null;
  }

  /**
   * Unattaches node from the dependency graph.
   * Node can still be reattached.
   */
  unattach(): void {
    this.destroyEdges();
    this.konvaGraphNode.remove();
  }

  copy(): GraphNode {
    const copyNode = new GraphNode(this._name, this._note);
    copyNode.konvaGraphNode = this.konvaGraphNode.copy();

    return copyNode;
  }

  configureEvents(configurer: EventConfigurer) {
    this.konvaGraphNode.configureEvents(this, configurer);
  }

  forSubgraph(func: (node: GraphNode) => void, edgePredicate?: (edge: GraphEdge) => boolean) {
    this._forSubgraphHelper(this, func, new Set<GraphNode>(), edgePredicate);
  }

  private _forSubgraphHelper(
    node: GraphNode,
    func: (node: GraphNode) => void,
    visited: Set<GraphNode>,
    edgePredicate?: (edge: GraphEdge) => boolean
  ): void {
    visited.add(node);
    func(node);

    let nextEdges: GraphEdge[] = node.childEdges;

    if (edgePredicate) {
      nextEdges = nextEdges.filter(edge => edgePredicate(edge));
    }

    for (const edge of nextEdges) {
      const currentNode = edge.childNode;

      if (!visited.has(currentNode)) {
        this._forSubgraphHelper(currentNode, func, visited, edgePredicate);
      }
    }
  }

  /**
   * Removes edge from the edge array.
   *
   * @param edgeArray Array of edges to remove from.
   * @param edge Edge to remove.
   */
  private _removeEdge(edgeArray: GraphEdge[], edge: GraphEdge): void {
    const edgeIndex = edgeArray.indexOf(edge);

    if (edgeIndex !== -1) {
      edgeArray.splice(edgeIndex, 1);
    }
  }
}
