import { DateTimeArgs } from '../../../models/interfaces/date-time-args';

export class DateTimeUtils {
  constructor() {}

  static dateFromDateTimeArgs(args: DateTimeArgs): Date | null {
    if ([args.day, args.hour, args.minute, args.month, args.second, args.year].every(val => val != null)) {
      const date = new Date();
      date.setFullYear(args.year, args.month - 1, args.day);
      date.setHours(args.hour, args.minute, args.second);
      return date;
    }
    return null;
  }
}
