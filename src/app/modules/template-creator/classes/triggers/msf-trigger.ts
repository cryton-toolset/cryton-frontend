import { TriggerType } from '../../models/enums/trigger-type';
import { MSFArgs } from '../../models/interfaces/msf-args';
import { Trigger } from './trigger';

export class MSFTrigger extends Trigger<MSFArgs> {
  constructor(args: MSFArgs) {
    super(args, TriggerType.MSF);
  }

  editArgs(triggerArgs: MSFArgs): void {
    this._args = Object.assign({}, triggerArgs);
  }

  getStartTime(): number {
    return;
  }

  setStartTime = (): void => {};
}
