import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { parse } from 'yaml';

export const validateYaml = (yaml: string): string | undefined => {
  if (!yaml) {
    return;
  }
  try {
    parse(yaml);
    return;
  } catch (err) {
    const typedErr = err as { message?: string };

    if (typedErr.message) {
      return 'Invalid YAML format: ' + typedErr.message;
    }
    return 'Invalid YAML format.';
  }
};

export const yamlValidator =
  (): ValidatorFn =>
  (control: AbstractControl): ValidationErrors => {
    if (!control.value) {
      return {};
    }
    return validateYaml(control.value) ? { invalidYaml: true } : {};
  };
