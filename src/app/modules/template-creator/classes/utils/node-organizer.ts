import { Queue } from 'src/app/shared/utils/queue';
import { Bounds } from '../../models/interfaces/bounds';
import { GraphEdge } from '../dependency-graph/edge/graph-edge';
import { GraphNode } from '../dependency-graph/node/graph-node';

const NODE_PADDING = 50;
const GRAPH_GAP = 50;

export class NodeOrganizer {
  // Primary axis for horizontal organizing is the X axis, for vertical the Y axis.
  private _primaryAxisGetter: (node: GraphNode) => number;
  private _primaryAxisSetter: (node: GraphNode, coord: number) => void;

  // Secondary axis for horizontal organizing is the Y axis, for horizontal the X axis.
  private _secondaryAxisGetter: (node: GraphNode) => number;
  private _secondaryAxisSetter: (node: GraphNode, coord: number) => void;

  private _primaryAxisLowerCoordGetter: (bounds: Bounds) => number;
  private _primaryAxisLowerCoordSetter: (bounds: Bounds, coord: number) => void;

  private _primaryAxisHigherCoordGetter: (bounds: Bounds) => number;
  private _primaryAxisHigherCoordSetter: (bounds: Bounds, coord: number) => void;

  private _secondaryAxisLowerCoordGetter: (bounds: Bounds) => number;
  private _secondaryAxisLowerCoordSetter: (bounds: Bounds, coord: number) => void;

  private _secondaryAxisHigherCoordGetter: (bounds: Bounds) => number;
  private _secondaryAxisHigherCoordSetter: (bounds: Bounds, coord: number) => void;

  constructor(private _direction: 'horizontal' | 'vertical', private _moveOnPrimaryAxis = true) {
    const xAxisGetter = (node: GraphNode) => node.konvaGraphNode.x;
    const xAxisSetter = (node: GraphNode, coord: number) => (node.konvaGraphNode.x = coord);

    const yAxisGetter = (node: GraphNode) => node.konvaGraphNode.y;
    const yAxisSetter = (node: GraphNode, coord: number) => (node.konvaGraphNode.y = coord);

    const leftGetter = (bounds: Bounds) => bounds.left;
    const leftSetter = (bounds: Bounds, coord: number) => (bounds.left = coord);

    const topGetter = (bounds: Bounds) => bounds.top;
    const topSetter = (bounds: Bounds, coord: number) => (bounds.top = coord);

    const rightGetter = (bounds: Bounds) => bounds.right;
    const rightSetter = (bounds: Bounds, coord: number) => (bounds.right = coord);

    const bottomGetter = (bounds: Bounds) => bounds.bottom;
    const bottomSetter = (bounds: Bounds, coord: number) => (bounds.bottom = coord);

    if (this._direction === 'horizontal') {
      this._primaryAxisGetter = xAxisGetter;
      this._primaryAxisSetter = xAxisSetter;

      this._secondaryAxisGetter = yAxisGetter;
      this._secondaryAxisSetter = yAxisSetter;

      this._primaryAxisLowerCoordGetter = leftGetter;
      this._primaryAxisLowerCoordSetter = leftSetter;

      this._primaryAxisHigherCoordGetter = rightGetter;
      this._primaryAxisHigherCoordSetter = rightSetter;

      this._secondaryAxisLowerCoordGetter = topGetter;
      this._secondaryAxisLowerCoordSetter = topSetter;

      this._secondaryAxisHigherCoordGetter = bottomGetter;
      this._secondaryAxisHigherCoordSetter = bottomSetter;
    } else {
      this._primaryAxisGetter = yAxisGetter;
      this._primaryAxisSetter = yAxisSetter;

      this._secondaryAxisGetter = xAxisGetter;
      this._secondaryAxisSetter = xAxisSetter;

      this._primaryAxisLowerCoordGetter = topGetter;
      this._primaryAxisLowerCoordSetter = topSetter;

      this._primaryAxisHigherCoordGetter = bottomGetter;
      this._primaryAxisHigherCoordSetter = bottomSetter;

      this._secondaryAxisLowerCoordGetter = leftGetter;
      this._secondaryAxisLowerCoordSetter = leftSetter;

      this._secondaryAxisHigherCoordGetter = rightGetter;
      this._secondaryAxisHigherCoordSetter = rightSetter;
    }
  }

  /**
   * Moves the entire graph starting from the root node by a given
   * increment on x and y axes.
   *
   * @param rootNode Root node of the graph.
   * @param xIncrement X increment.
   * @param yIncrement Y increment.
   * @param moveRoot Specifies if root node should be moved as well.
   * @param edgePredicate Predicate function for filtering nodes to be visited.
   */
  moveGraph(
    rootNode: GraphNode,
    xIncrement: number,
    yIncrement: number,
    moveRoot = false,
    edgePredicate?: (edge: GraphEdge) => boolean
  ): Bounds {
    return this._moveGraphHelper(rootNode, xIncrement, yIncrement, moveRoot ? null : rootNode, edgePredicate);
  }

  /**
   * Helper method for the moveGraph method. Does the actual moving of the graph.
   *
   * @param rootNode Root node of the graph.
   * @param xIncrement X increment.
   * @param yIncrement Y increment.
   * @param dontMoveNode A node that shouldn't be moved (used for root node of the entire graph).
   * @param edgePredicate Predicate function for filtering nodes to be visited.
   * @param visited Set of visited nodes for cycle detection.
   */
  private _moveGraphHelper(
    rootNode: GraphNode,
    xIncrement: number,
    yIncrement: number,
    dontMoveNode: GraphNode,
    edgePredicate?: (edge: GraphEdge) => boolean,
    visited = new Set<GraphNode>(),
    bounds = { left: Infinity, right: -Infinity, top: Infinity, bottom: -Infinity }
  ): Bounds {
    visited.add(rootNode);

    if (rootNode !== dontMoveNode) {
      rootNode.konvaGraphNode.x += xIncrement;
      rootNode.konvaGraphNode.y += yIncrement;
    }
    bounds = this._updateBounds(bounds, this._calcNodeBounds(rootNode));

    let nextEdges: GraphEdge[] = rootNode.childEdges;

    if (edgePredicate) {
      nextEdges = nextEdges.filter(edge => edgePredicate(edge));
    }

    for (const edge of nextEdges) {
      const currentNode = edge.childNode;

      if (!visited.has(currentNode)) {
        const nextBounds = this._moveGraphHelper(
          currentNode,
          xIncrement,
          yIncrement,
          dontMoveNode,
          edgePredicate,
          visited,
          bounds
        );
        bounds = this._updateBounds(bounds, nextBounds);
      }
    }

    return bounds;
  }

  /**
   * Organizes a graph defined by the root node.
   *
   * @param rootNode Root node of the graph.
   * @returns Bounds of the graph.
   */
  organizeGraph(rootNode: GraphNode): Bounds {
    return this._organizeGraph(rootNode, -Infinity, Infinity);
  }

  /**
   * Organizes individual graphs in the plan and places them next to each other with a small gap in between.
   *
   * @param nodes Array of nodes to be organized.
   * @returns Bounds of the entire plan.
   */
  organizeNodes(nodes: GraphNode[]): Bounds {
    if (nodes.length === 0) {
      return { left: 0, right: 0, top: 0, bottom: 0 };
    }

    const graphBoundsMap = new Map<GraphNode, Bounds>();

    nodes.forEach(node => {
      this._secondaryAxisSetter(node, 0);
    });

    const rootNodes = nodes.filter(node => node.parentEdges.length === 0);

    let totalSecondaryAxisSize = -GRAPH_GAP;

    // Calculate total size on secondary axis of all graphs added together with a gap between them.
    rootNodes.forEach(node => {
      const graphBounds = this.organizeGraph(node);
      graphBoundsMap.set(node, graphBounds);

      totalSecondaryAxisSize +=
        this._secondaryAxisHigherCoordGetter(graphBounds) -
        this._secondaryAxisLowerCoordGetter(graphBounds) +
        GRAPH_GAP;
    });

    let lastLowerBound = -totalSecondaryAxisSize / 2;
    let totalBounds = { left: 0, right: 0, top: 0, bottom: 0 };

    // Move graphs to their correct position starting from the lower coordinate.
    rootNodes.forEach(node => {
      const graphBounds = graphBoundsMap.get(node);

      const lowerAxisCoord = this._secondaryAxisLowerCoordGetter(graphBounds);
      const offset = lastLowerBound - lowerAxisCoord;

      let movedBounds: Bounds;

      if (this._direction === 'horizontal') {
        movedBounds = this.moveGraph(node, 0, offset, true, edge =>
          this._isNodeLastParent(edge.parentNode, edge.childNode)
        );
      } else {
        movedBounds = this.moveGraph(node, offset, 0, true, edge =>
          this._isNodeLastParent(edge.parentNode, edge.childNode)
        );
      }

      lastLowerBound = this._secondaryAxisHigherCoordGetter(movedBounds) + GRAPH_GAP;
      totalBounds = this._updateBounds(totalBounds, movedBounds);
    });

    this._updateDepGraphEdges(nodes);

    return totalBounds;
  }

  /**
   * Organizes a graph which starts at the given root node.
   *
   * @param rootNode Root node of the graph.
   * @param minCoord Minimum coordinate where tree bounds can reach.
   * @param maxCoord Maximum coordinate where graph bounds can reach.
   * @returns Bounds of organized graph.
   */
  private _organizeGraph(rootNode: GraphNode, minCoord: number, maxCoord: number): Bounds {
    // Place the root node to its correct position.
    this._setRootNodeCoord(rootNode, minCoord, maxCoord);

    // Only last parent should calc Y position for better structure
    // Sort edges by child node name for deterministic result
    const childEdges = this._filterSortEdges(rootNode);

    // Moves root node on the main axis to create space between the node and it's last parent.
    if (this._moveOnPrimaryAxis && rootNode.parentEdges.length > 0) {
      const lastParentBounds = this._calcNodeBounds(this._getNodesLastParent(rootNode));
      this._primaryAxisSetter(rootNode, this._primaryAxisHigherCoordGetter(lastParentBounds) + NODE_PADDING);
    }

    // Bounds for the centered part of the graph.
    let centeredGraphBounds = this._calcNodeBounds(rootNode);
    let currentBounds = Object.assign({}, centeredGraphBounds);

    if (childEdges.length === 0) {
      return currentBounds;
    }

    // If there is an odd number of children, place one child in the middle and the other children around it.
    // If there is an even number of children, move bounds closer to the middle to create even padding between all children.
    if (childEdges.length % 2 === 1) {
      const firstChild = childEdges[0].childNode;

      // Center first child.
      const widthDiff = rootNode.konvaGraphNode.getWidth() - firstChild.konvaGraphNode.getWidth();
      this._secondaryAxisSetter(firstChild, this._secondaryAxisGetter(rootNode) + widthDiff / 2);

      // Organize first childs subhraph.
      centeredGraphBounds = this._organizeGraph(firstChild, -Infinity, Infinity);
      currentBounds = this._updateBounds(currentBounds, centeredGraphBounds);

      // Add padding if there are gonna be more children around the middle node.
      this._secondaryAxisLowerCoordSetter(
        centeredGraphBounds,
        this._secondaryAxisLowerCoordGetter(centeredGraphBounds) - NODE_PADDING
      );
      this._secondaryAxisHigherCoordSetter(
        centeredGraphBounds,
        this._secondaryAxisHigherCoordGetter(centeredGraphBounds) + NODE_PADDING
      );
    } else {
      const rootNodeCenterCoord =
        (this._secondaryAxisLowerCoordGetter(centeredGraphBounds) +
          this._secondaryAxisHigherCoordGetter(centeredGraphBounds)) /
        2;
      this._secondaryAxisLowerCoordSetter(centeredGraphBounds, rootNodeCenterCoord - NODE_PADDING / 2);
      this._secondaryAxisHigherCoordSetter(centeredGraphBounds, rootNodeCenterCoord + NODE_PADDING / 2);
    }

    if (childEdges.length > 1) {
      // First node was placed in the middle when we have odd number of children.
      const startIndex = childEdges.length % 2 === 1 ? 1 : 0;
      const middleIndex = Math.floor((childEdges.length - startIndex) / 2) + startIndex;

      let lastBounds = Object.assign({}, centeredGraphBounds);

      // Place first half of the children on one side of the graph.
      for (let i = startIndex; i < middleIndex; i++) {
        lastBounds = this._organizeGraph(
          childEdges[i].childNode,
          -Infinity,
          this._secondaryAxisLowerCoordGetter(lastBounds)
        );

        // Recalculate bounds of the entire graph.
        currentBounds = this._updateBounds(currentBounds, lastBounds);

        // Add padding for the next node.
        this._secondaryAxisLowerCoordSetter(lastBounds, this._secondaryAxisLowerCoordGetter(lastBounds) - NODE_PADDING);
      }

      lastBounds = Object.assign({}, centeredGraphBounds);

      // Place the second half of the children on the other side of the graph.
      for (let i = middleIndex; i < childEdges.length; i++) {
        lastBounds = this._organizeGraph(
          childEdges[i].childNode,
          this._secondaryAxisHigherCoordGetter(lastBounds),
          Infinity
        );

        // Recalculate bounds of the entire graph.
        currentBounds = this._updateBounds(currentBounds, lastBounds);

        // Add padding for the next node.
        this._secondaryAxisHigherCoordSetter(
          lastBounds,
          this._secondaryAxisHigherCoordGetter(lastBounds) + NODE_PADDING
        );
      }
    }

    // Check if the tree exceeded min/max coordinate and move it if needed.
    currentBounds = this._checkBounds(rootNode, currentBounds, minCoord, maxCoord);

    return currentBounds;
  }

  /**
   * Calculates bounds of a given node.
   *
   * @param node Node to calculate bounds of.
   * @returns Bounds of a given node.
   */
  private _calcNodeBounds(node: GraphNode): Bounds {
    const nodeHeight = node.konvaGraphNode.getHeight();
    const nodeWidth = node.konvaGraphNode.getWidth();

    return {
      top: node.konvaGraphNode.y,
      bottom: node.konvaGraphNode.y + nodeHeight,
      left: node.konvaGraphNode.x,
      right: node.konvaGraphNode.x + nodeWidth
    };
  }

  /**
   * Compares all coordinates of bounds and updates the values
   * to the min/max coordinate.
   *
   * @param bounds Bounds to be updated.
   * @param newBounds Bounds to compare with.
   * @returns Updated bounds.
   */
  private _updateBounds(bounds: Bounds, newBounds: Bounds): Bounds {
    const updatedBounds = { left: 0, right: 0, top: 0, bottom: 0 };

    updatedBounds.top = Math.min(bounds.top, newBounds.top);
    updatedBounds.bottom = Math.max(bounds.bottom, newBounds.bottom);
    updatedBounds.right = Math.max(bounds.right, newBounds.right);
    updatedBounds.left = Math.min(bounds.left, newBounds.left);

    return updatedBounds;
  }

  /**
   * Sets the root node coordinate based on minimal and maximal coordinate.
   *
   * @param rootNode Root node of a graph.
   * @param maxCoord Maximal coordinate.
   * @param minCoord Minimal coordinate.
   */
  private _setRootNodeCoord(rootNode: GraphNode, minCoord: number, maxCoord: number): void {
    if (maxCoord !== Infinity) {
      this._secondaryAxisSetter(
        rootNode,
        this._direction === 'horizontal'
          ? maxCoord - rootNode.konvaGraphNode.getHeight()
          : maxCoord - rootNode.konvaGraphNode.getWidth()
      );
    } else if (minCoord !== -Infinity) {
      this._secondaryAxisSetter(rootNode, this._direction === 'horizontal' ? minCoord : minCoord);
    }
  }

  /**
   * Filters out all edges to nodes of which the root node isn't the last parent.
   * Sorts the remaining nodes lexicographically by name.
   *
   * @param node Node whoose edges are processed.
   * @returns Array of processed nodes.
   */
  private _filterSortEdges(node: GraphNode): GraphEdge[] {
    const edges = node.childEdges;

    return edges
      .filter(edge => this._isNodeLastParent(node, edge.childNode))
      .sort((a: GraphEdge, b: GraphEdge) => {
        const aName = a.childNode.name;
        const bName = b.childNode.name;

        return aName === bName ? 0 : aName < bName ? -1 : 1;
      });
  }

  private _getNodesLastParent(node: GraphNode): GraphNode | null {
    const parents = node.parentEdges.map(edge => edge.parentNode);
    let lastParent = null;
    let maxDist = -1;

    parents.forEach(parent => {
      const distFromRoot = this._nodeDistFromRoot(parent);

      if (maxDist < distFromRoot) {
        maxDist = distFromRoot;
        lastParent = parent;
      } else if (maxDist === distFromRoot && parent.name < lastParent.name) {
        lastParent = parent;
      }
    });

    return lastParent;
  }

  /**
   * Decides if a node is the last parent of a child node.
   * Parent with the longest distance from the root node is the last parent.
   *
   * @param parentNode Parent node.
   * @param childNode Child node.
   * @returns True if node is the last parent.
   */
  private _isNodeLastParent(parentNode: GraphNode, childNode: GraphNode): boolean {
    const lastParent = this._getNodesLastParent(childNode);
    return parentNode === lastParent;
  }

  /**
   * Calculates node's longest distance from the root node.
   *
   * @param node Node to calculate the distance of.
   * @returns Distance from the root.
   */
  private _nodeDistFromRoot(node: GraphNode): number {
    const queue = new Queue<GraphNode>();
    const distances = new Map<string, number>();
    let maxDist = 0;

    distances.set(node.name, 0);
    queue.enqueue(node);

    while (!queue.isEmpty()) {
      const currentNode = queue.dequeue();
      const currentDist = distances.get(currentNode.name);

      currentNode.parentEdges.forEach((parentEdge: GraphEdge) => {
        const parent = parentEdge.parentNode;

        if (!distances.has(parent.name)) {
          distances.set(parent.name, distances.get(currentNode.name) + 1);
          queue.enqueue(parent);
        }
        if (parent.parentEdges.length === 0 && currentDist + 1 > maxDist) {
          maxDist = currentDist + 1;
        }
      });
    }

    return maxDist;
  }

  /**
   * Checks if the bounds exceed the max/min coordinates and if they do,
   * moves the graph starting at the root node accordingly.
   *
   * @param rootNode Root node of a graph with the given bounds.
   * @param graphBounds Bounds of a graph with a given root node.
   * @param minCoord Maximal coordinate of the graph bounds.
   * @param maxCoord Minimal coordinate of the graph bounds.
   * @returns Bounds of the moved graph.
   */
  private _checkBounds(rootNode: GraphNode, graphBounds: Bounds, minCoord: number, maxCoord: number): Bounds {
    let increment = 0;

    const secondaryHigherBounds = this._secondaryAxisHigherCoordGetter(graphBounds);
    const secondaryLowerBounds = this._secondaryAxisLowerCoordGetter(graphBounds);

    if (secondaryHigherBounds > maxCoord) {
      increment = -(secondaryHigherBounds - maxCoord);
    } else if (secondaryLowerBounds < minCoord) {
      increment = minCoord - secondaryLowerBounds;
    }

    if (increment !== 0) {
      const newBounds =
        this._direction === 'horizontal'
          ? this.moveGraph(rootNode, 0, increment, true)
          : this.moveGraph(rootNode, increment, 0, true);

      return newBounds;
    }

    return graphBounds;
  }

  private _updateDepGraphEdges(nodes: GraphNode[]): void {
    nodes.forEach(node => {
      node.childEdges.forEach(edge => {
        edge.konvaGraphEdge.refreshTracking();
      });
    });
  }
}
