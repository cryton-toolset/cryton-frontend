import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimelineComponent } from './components/timeline/timeline.component';
import { CreateRunPageComponent } from './pages/create-run-page/create-run-page.component';
import { ListRunsPageComponent } from './pages/list-runs-page/list-runs-page.component';
import { RunYamlPageComponent } from './pages/run-yaml-page/run-yaml-page.component';
import { RunPageComponent } from './pages/run/run-page.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListRunsPageComponent
  },
  {
    path: 'create',
    component: CreateRunPageComponent
  },
  {
    path: ':id',
    component: RunPageComponent
  },
  {
    path: ':id/timeline',
    component: TimelineComponent
  },
  {
    path: ':id/yaml',
    component: RunYamlPageComponent
  },
  {
    path: '',
    redirectTo: '/app/runs/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RunRoutingModule {}
