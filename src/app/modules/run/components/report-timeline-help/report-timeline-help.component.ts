import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RunState } from 'src/app/models/types/run-state.type';

interface ColorGroup {
  colorModifier: string;
  states: RunState[];
}

@Component({
  selector: 'app-report-timeline-help',
  templateUrl: './report-timeline-help.component.html',
  styleUrls: ['./report-timeline-help.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReportTimelineHelpComponent {
  colorGroups: ColorGroup[] = [
    { colorModifier: 'down', states: ['PAUSED', 'PAUSING'] },
    { colorModifier: 'running', states: ['RUNNING'] },
    { colorModifier: 'pending', states: ['PENDING', 'WAITING', 'AWAITING'] },
    { colorModifier: 'success', states: ['FINISHED'] },
    { colorModifier: 'error', states: ['ERROR', 'IGNORE', 'TERMINATED'] }
  ];

  constructor() {}
}
