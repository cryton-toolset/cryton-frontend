import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError, filter, finalize, first, switchMap } from 'rxjs/operators';
import { ExecutionVariableService } from 'src/app/core/http/execution-variable/execution-variable.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { ExecutionVariable } from 'src/app/models/api-responses/execution-variable.interface';
import { PlanExecutionReport } from 'src/app/models/api-responses/report/plan-execution-report.interface';
import { TableData } from 'src/app/models/api-responses/table-data.interface';
import { runStateColorMap } from 'src/app/models/maps/run-state-color.map';
import { CrytonInventoryCreatorComponent } from 'src/app/shared/components/cryton-inventory-creator/cryton-inventory-creator.component';
import { TableFilter } from 'src/app/shared/components/cryton-table/models/table-filter.interface';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-execution-report-card',
  templateUrl: './execution-report-card.component.html',
  styleUrls: ['../../styles/report.scss', './execution-report-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionReportCardComponent {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  // Array of loaded execution variables.
  variables?: TableData<ExecutionVariable>;

  // Emits true if the component is currently making an asynchronous request.
  loading$ = new BehaviorSubject<boolean>(false);

  // Says if the first variable load had already happened.
  initialized = false;

  runStateColorMap = runStateColorMap;
  variablePageSize = 4;

  private _execution: PlanExecutionReport;

  constructor(
    private _variableService: ExecutionVariableService,
    private _alert: AlertService,
    private _dialog: MatDialog
  ) {}

  get execution(): PlanExecutionReport {
    return this._execution;
  }

  @Input() set execution(value: PlanExecutionReport) {
    this._execution = value;
    this.loadVariables();
  }

  createVariables(): void {
    const variableDialog = this._dialog.open(CrytonInventoryCreatorComponent);

    variableDialog
      .afterClosed()
      .pipe(
        first(),
        filter((yaml: string) => {
          if (yaml) {
            return true;
          }
        }),
        switchMap((yaml: string) => {
          this.loading$.next(true);
          return this._variableService.postVariablesYaml(this.execution.id, yaml);
        })
      )
      .subscribe({
        next: () => {
          this.refreshVariables();
          this._alert.showSuccess('Variables uploaded successfully.');
        },
        error: () => {
          this.loading$.next(false);
          this._alert.showError('Failed to upload variables.');
        }
      });
  }

  onVariableDelete(): void {
    if (this.variables && this.variables.results.length === 1 && this.paginator.pageIndex > 0) {
      this.paginator.previousPage();
    } else {
      this.refreshVariables();
    }
  }

  refreshVariables(): void {
    const { pageSize, pageIndex } = this.paginator;

    this.loading$.next(true);
    setTimeout(() => this.loadVariables(pageSize * pageIndex, pageSize), environment.refreshDelay);
  }

  /**
   * Loads execution variables from the backend. Handles loading observable emission and alert emission.
   */
  loadVariables(offset = 0, limit = this.variablePageSize): void {
    this.loading$.next(true);

    const executionFilter: TableFilter = { column: 'plan_execution_id', filter: this._execution.id.toString() };

    this._variableService
      .fetchItems(offset, limit, null, executionFilter)
      .pipe(
        first(),
        catchError(() => throwError(() => new Error('Loading execution variables failed.'))),
        finalize(() => this.loading$.next(false))
      )
      .subscribe({
        next: (vars: TableData<ExecutionVariable>) => {
          this.variables = vars;
          this.initialized = true;
        },
        error: (err: Error) => {
          this._alert.showError(err.message);
        }
      });
  }

  /**
   * Tracking function for *ngFor directive.
   *
   * @param _ Item index (not needed).
   * @param item Tracked item
   * @returns Unique dentifier.
   */
  trackByFn(_: number, item: ExecutionVariable): number {
    return item.id;
  }

  onPage({ pageIndex, pageSize }: PageEvent): void {
    this.loadVariables(pageIndex * pageSize, pageSize);
  }
}
