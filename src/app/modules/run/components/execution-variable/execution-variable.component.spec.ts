import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { of } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { ExecutionVariable } from 'src/app/models/api-responses/execution-variable.interface';
import { SharedModule } from 'src/app/shared/shared.module';
import { alertServiceStub } from 'src/app/testing/stubs/alert-service.stub';
import { httpClientStub } from 'src/app/testing/stubs/http-client.stub';
import { ExecutionVariableComponent } from './execution-variable.component';

describe('ExecutionVariableComponent', () => {
  let component: ExecutionVariableComponent;
  let fixture: ComponentFixture<ExecutionVariableComponent>;

  const certainityCheck = {
    afterClosed: () => of(true)
  };
  const dialogStub = {
    open: () => certainityCheck
  };
  const mockVariable: ExecutionVariable = {
    id: 1,
    created_at: '2020-08-24T12:22:55.909039',
    updated_at: '2020-08-24T12:22:56.024864',
    name: 'target',
    value: '127.0.0.1',
    plan_execution: 1
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExecutionVariableComponent],
      imports: [SharedModule, MatIconModule],
      providers: [
        { provide: HttpClient, useValue: httpClientStub },
        { provide: MatDialog, useValue: dialogStub },
        { provide: AlertService, useValue: alertServiceStub }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionVariableComponent);
    component = fixture.componentInstance;
    component.variable = mockVariable;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
