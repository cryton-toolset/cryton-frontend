import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { catchError, delay, first, switchMapTo } from 'rxjs/operators';
import { RunService } from 'src/app/core/http/run/run.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { Report } from 'src/app/models/api-responses/report/report.interface';

@Component({
  selector: 'app-run-page',
  templateUrl: './run-page.component.html',
  styleUrls: ['./run-page.component.scss', '../../styles/report.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RunPageComponent implements OnInit {
  report$ = new BehaviorSubject<Report>(null);
  loading$ = new BehaviorSubject<boolean>(false);
  runID: number;

  constructor(private _route: ActivatedRoute, private _runService: RunService, private _alert: AlertService) {}

  ngOnInit(): void {
    this.runID = Number(this._route.snapshot.paramMap.get('id'));
    this.loadReport();
  }

  downloadReport(): void {
    this._runService.downloadReport(this.runID);
  }

  loadReport(): void {
    this.loading$.next(true);
    of({})
      .pipe(
        first(),
        delay(200),
        switchMapTo(this._runService.fetchReport(this.runID).pipe(first())),
        catchError(() => throwError(() => new Error('Fetching report failed.')))
      )
      .subscribe({
        next: report => {
          this.loading$.next(false);
          this.report$.next(report);
        },
        error: err => {
          this.loading$.next(false);
          this._alert.showError(err);
        }
      });
  }
}
