import { Component } from '@angular/core';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { StepOverviewItem } from 'src/app/shared/components/cryton-editor/models/step-overview-item.interface';
import { StepType } from 'src/app/shared/components/cryton-editor/models/step-type.enum';
import { RunCreationStepsComponent } from 'src/app/shared/components/cryton-editor/steps/run-creation-steps/run-creation-steps.component';

@Component({
  selector: 'app-create-run-page',
  templateUrl: './create-run-page.component.html',
  animations: [renderComponentTrigger]
})
export class CreateRunPageComponent {
  editorSteps = RunCreationStepsComponent;
  stepOverviewItems: StepOverviewItem[] = [
    { name: 'Plan', type: StepType.SELECTABLE, required: true },
    { name: 'Workers', type: StepType.SELECTABLE, required: true },
    { name: 'Execution variables', type: StepType.SELECTABLE, required: false }
  ];

  constructor() {}
}
