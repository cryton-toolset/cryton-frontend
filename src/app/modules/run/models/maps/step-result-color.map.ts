import { StepResult } from '../enums/step-result.enum';

export const stepResultColorMap = new Map<StepResult, string>([
  [StepResult.OK, 'success'],
  [StepResult.FAIL, 'error']
]);
