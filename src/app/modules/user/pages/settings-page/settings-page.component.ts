import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { BehaviorSubject, first } from 'rxjs';
import { BackendStatusService } from 'src/app/core/http/backend-status/backend-status.service';
import { CrytonRESTApiService } from 'src/app/core/http/cryton-rest-api-service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { environment } from 'src/environments/environment';
import { getControlError } from './settings-page.errors';

const URL_REGEX = /^https?:\/\/\w+(\.\w+)*(:[0-9]+)?\/?(\/[.\w]*)*$/;

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsPageComponent implements OnInit {
  settingsForm = new FormGroup({
    restApiUrl: new FormControl({ value: '', disabled: true }, [Validators.required]),
    useCustomUrl: new FormControl(false)
  });
  loading$ = new BehaviorSubject(false);
  getControlError = getControlError;

  constructor(private _backendStatus: BackendStatusService, private _alert: AlertService) {}

  ngOnInit(): void {
    this._initBaseUrlSetting();
  }

  toggleApiUrlInput(event: MatCheckboxChange): void {
    const restApiUrlCtrl = this.settingsForm.get('restApiUrl');

    if (event.checked) {
      restApiUrlCtrl.enable();
    } else {
      restApiUrlCtrl.disable();
    }
  }

  applySettings(): void {
    this.loading$.next(true);

    setTimeout(() => {
      this._applyBaseUrlSetting();
      this.loading$.next(false);
      this._alert.showSuccess('Settings saved.');
    }, environment.refreshDelay);
  }

  private _initBaseUrlSetting(): void {
    const { restApiUrl, useCustomUrl } = this.settingsForm.controls;

    restApiUrl.setValue(CrytonRESTApiService.baseUrl);
    restApiUrl.addValidators(this._baseUrlValidator());

    if (window.localStorage.getItem('customBaseUrl')) {
      useCustomUrl.setValue(true);
      restApiUrl.enable();
    }
  }

  private _applyBaseUrlSetting(): void {
    const { restApiUrl, useCustomUrl } = this.settingsForm.value;

    if (useCustomUrl) {
      const withTrailingSlash = restApiUrl.endsWith('/') ? restApiUrl : restApiUrl + '/';
      window.localStorage.setItem('customBaseUrl', withTrailingSlash);
    } else {
      window.localStorage.removeItem('customBaseUrl');
    }
    this._backendStatus.checkBackendStatus().pipe(first()).subscribe();
  }

  private _baseUrlValidator =
    (): ValidatorFn =>
    (control: AbstractControl): ValidationErrors => {
      const url: string = control.value;
      return URL_REGEX.test(url) ? {} : { invalidUrl: true };
    };
}
