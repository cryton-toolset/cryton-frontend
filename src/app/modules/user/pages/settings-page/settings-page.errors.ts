import { FormGroup } from '@angular/forms';

const ERROR_MESSAGES: Record<string, Record<string, string>> = {
  restApiUrl: {
    required: 'Value is required.',
    invalidUrl: 'URL format is invalid.'
  }
};

export const getControlError = (formGroup: FormGroup, controlName: string): string => {
  const errors = formGroup.get(controlName).errors;

  return errors ? ERROR_MESSAGES[controlName][Object.keys(errors)[0]] : null;
};
