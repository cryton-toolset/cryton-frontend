import { ChangeDetectionStrategy, Component, Input, OnDestroy } from '@angular/core';
import { catchError, first, Subject, switchMapTo, tap, throwError } from 'rxjs';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { WorkerState } from 'src/app/models/types/worker-state.type';
import { HealthCheckButton } from 'src/app/shared/components/cryton-table/buttons/healthcheck-button';
import { Worker } from '../../../../models/api-responses/worker.interface';

const STATE_COLOR_MAP = new Map<WorkerState, string>([
  ['UP', 'running'],
  ['DOWN', 'down'],
  ['READY', 'pending']
]);

@Component({
  selector: 'app-worker-table',
  templateUrl: './worker-table.component.html',
  styleUrls: ['./worker-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkerTableComponent implements OnDestroy {
  @Input() data: Worker;

  maxStringLength = 40;
  healthCheckBtn: HealthCheckButton;

  private _destroySubject$ = new Subject<void>();

  constructor(private _workerSevice: WorkersService, private _alert: AlertService) {
    this.healthCheckBtn = new HealthCheckButton(this._workerSevice);
  }

  ngOnDestroy(): void {
    this._destroySubject$.next();
    this._destroySubject$.complete();
  }

  getStateClass(state: WorkerState): string {
    return `state--${STATE_COLOR_MAP.get(state)}`;
  }

  getTooltip(text: string): string {
    return text.length > this.maxStringLength ? text : null;
  }

  healthCheck(): void {
    this.healthCheckBtn
      .executeAction(this.data)
      .pipe(
        first(),
        tap(msg => this._alert.showSuccess(msg)),
        catchError(err => {
          this._alert.showError(err);
          return throwError(() => new Error(err));
        }),
        switchMapTo(this.healthCheckBtn.rowUpdate$)
      )
      .subscribe(newData => (this.data = newData));
  }
}
