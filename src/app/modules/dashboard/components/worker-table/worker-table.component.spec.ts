import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { AlertService } from 'src/app/core/services/alert/alert.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { SharedModule } from 'src/app/shared/shared.module';
import { alertServiceStub } from 'src/app/testing/stubs/alert-service.stub';
import { Spied } from 'src/app/testing/utility/utility-types';
import { WorkerTableComponent } from './worker-table.component';

describe('WorkerTableComponent', () => {
  let component: WorkerTableComponent;
  let fixture: ComponentFixture<WorkerTableComponent>;

  const workerServiceStub = jasmine.createSpyObj('WorkerService', ['healthCheck']) as Spied<WorkersService>;
  workerServiceStub.healthCheck.and.returnValue({ detail: { worker_model_id: 1, worker_state: 'DOWN' } });

  const worker: Worker = {
    id: 1,
    created_at: '2020-08-24T12:22:55.909039',
    updated_at: '2020-08-24T12:22:56.024864',
    name: 'Hard Worker',
    description: 'Worker description',
    state: 'UP'
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        MatPaginatorModule,
        MatTooltipModule,
        MatIconModule,
        SharedModule,
        BrowserAnimationsModule,
        MatDividerModule
      ],
      declarations: [WorkerTableComponent],
      providers: [
        { provide: WorkersService, useValue: workerServiceStub },
        { provide: AlertService, useValue: alertServiceStub }
      ]
    })
      .overrideComponent(WorkerTableComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();

    fixture = TestBed.createComponent(WorkerTableComponent);
    component = fixture.componentInstance;
    component.data = worker;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display UP as the state of the first worker', () => {
    const elementRef = fixture.nativeElement as HTMLElement;
    const state = elementRef.querySelector('.worker__state-name');
    expect(state.innerHTML).toBe('UP');
  });

  it('should display 1 as the RUN ID', () => {
    const elementRef = fixture.nativeElement as HTMLElement;
    const runID = elementRef.querySelector('.worker__id-text');
    expect(runID.innerHTML).toContain('1');
  });

  it('should return correct class for each state', () => {
    const componentRef = fixture.componentInstance;

    expect(componentRef.getStateClass('UP')).toBe('state--running');
    expect(componentRef.getStateClass('DOWN')).toBe('state--down');
    expect(componentRef.getStateClass('READY')).toBe('state--pending');
  });
});
