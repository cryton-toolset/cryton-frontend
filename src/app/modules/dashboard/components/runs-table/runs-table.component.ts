import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RunService } from 'src/app/core/http/run/run.service';
import { Run } from 'src/app/models/api-responses/run.interface';
import { LinkButton } from 'src/app/shared/components/cryton-table/buttons/link-button';
import { TableButton } from 'src/app/shared/components/cryton-table/buttons/table-button';
import { RunTableDataSource } from 'src/app/shared/components/cryton-table/data-source/run-table.data-source';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';

@Component({
  selector: 'app-runs-table',
  templateUrl: './runs-table.component.html'
})
export class RunsTableComponent implements OnInit {
  dataSource: RunTableDataSource;
  buttons: TableButton[];

  constructor(private _runService: RunService, private _crytonDatetime: CrytonDatetimePipe, private _router: Router) {}

  ngOnInit(): void {
    this.dataSource = new RunTableDataSource(this._runService, this._crytonDatetime);
    this.buttons = [
      new LinkButton('Show run', 'visibility', '/app/runs/:id'),
      new LinkButton('Show timeline', 'schedule', '/app/runs/:id/timeline'),
      new LinkButton('Show YAML', 'description', '/app/runs/:id/yaml')
    ];
  }

  viewRun = (run: Run): Observable<string> => {
    this._router.navigate(['app', 'runs', run.id]);
    return of(null) as Observable<string>;
  };
}
