import { Component, OnInit } from '@angular/core';
import { PlanService } from 'src/app/core/http/plan/plan.service';
import { LinkButton } from 'src/app/shared/components/cryton-table/buttons/link-button';
import { TableButton } from 'src/app/shared/components/cryton-table/buttons/table-button';
import { PlanTableDataSource } from 'src/app/shared/components/cryton-table/data-source/plan-table.data-source';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';

@Component({
  selector: 'app-plans-table',
  templateUrl: './plans-table.component.html'
})
export class PlansTableComponent implements OnInit {
  dataSource: PlanTableDataSource;
  buttons: TableButton[];
  searchValue = '';

  constructor(private _planService: PlanService, private _datePipe: CrytonDatetimePipe) {}

  ngOnInit(): void {
    this.dataSource = new PlanTableDataSource(this._planService, this._datePipe);
    this.buttons = [new LinkButton('Show YAML', 'description', '/app/plans/:id/yaml')];
  }
}
