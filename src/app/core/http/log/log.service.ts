import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CrytonResponse } from 'src/app/models/api-responses/cryton-response.interface';
import { Log } from 'src/app/models/api-responses/log.interface';
import { Endpoint } from 'src/app/models/enums/endpoint.enum';
import { CrytonRESTApiService } from '../cryton-rest-api-service';

export type LogsResponse = CrytonResponse<Log>;

@Injectable({
  providedIn: 'root'
})
export class LogService {
  get endpoint(): string {
    return CrytonRESTApiService.buildEndpointURL(Endpoint.LOGS);
  }

  constructor(private _http: HttpClient) {}

  fetchItems(offset = 0, limit = 0, filter = ''): Observable<LogsResponse> {
    const params: HttpParams = new HttpParams()
      .set('offset', offset?.toString() ?? '0')
      .set('limit', limit?.toString() ?? '0')
      .set('filter', filter ?? '');

    return this._http.get<LogsResponse>(this.endpoint, { params });
  }
}
