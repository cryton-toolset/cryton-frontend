import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Plan } from 'src/app/models/api-responses/plan.interface';
import { Endpoint } from 'src/app/models/enums/endpoint.enum';
import { HasYaml } from 'src/app/models/interfaces/has-yaml.interface';
import { PlanDescription } from 'src/app/modules/template-creator/models/interfaces/template-description';
import { CrytonRESTApiService } from '../cryton-rest-api-service';

@Injectable({
  providedIn: 'root'
})
export class PlanService extends CrytonRESTApiService<Plan> implements HasYaml {
  get endpoint(): string {
    return CrytonRESTApiService.buildEndpointURL(Endpoint.PLANS);
  }

  constructor(protected http: HttpClient) {
    super(http);
  }

  postPlan(templateID: number, inventory: File[] | string): Observable<string> {
    const formData = new FormData();

    formData.append('template_id', templateID.toString());

    if (inventory) {
      if (Array.isArray(inventory)) {
        for (const file of inventory) {
          formData.append('inventory_file', file, 'inventory_file');
        }
      } else {
        formData.append('inventory_file', inventory);
      }
    }

    return this.http.post(this.endpoint, formData).pipe(
      map(() => 'Plan created successfully.'),
      catchError((err: HttpErrorResponse) => this.handleItemError(err, 'Plan creation failed.'))
    );
  }

  fetchYaml(planID: number): Observable<{ detail: PlanDescription }> {
    return this.http.get<{ detail: PlanDescription }>(`${this.endpoint}${planID}/get_plan`);
  }
}
