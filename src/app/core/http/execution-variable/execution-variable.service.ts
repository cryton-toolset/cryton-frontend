import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, mapTo } from 'rxjs/operators';
import { ExecutionVariable } from 'src/app/models/api-responses/execution-variable.interface';
import { Endpoint } from 'src/app/models/enums/endpoint.enum';
import { CrytonRESTApiService } from '../cryton-rest-api-service';

@Injectable({
  providedIn: 'root'
})
export class ExecutionVariableService extends CrytonRESTApiService<ExecutionVariable> {
  get endpoint(): string {
    return CrytonRESTApiService.buildEndpointURL(Endpoint.EXECUTION_VARS);
  }

  constructor(protected http: HttpClient) {
    super(http);
  }

  /**
   * Uploads files containing execution variable definitions to the backend.
   *
   * @param executionId ID of execution to which the variables belong.
   * @param files Array of files defining the execution variables.
   * @returns Observable of the response.
   */
  postVariablesFiles(executionId: number, files: File[]): Observable<string> {
    const formData = this._createFormData(executionId, files);
    return this.http.post<{ detail: string }>(this.endpoint, formData).pipe(
      catchError((err: HttpErrorResponse) => this.handleItemError(err, 'Variables upload failed.')),
      mapTo('Variables uploaded successfully.')
    );
  }

  /**
   * Uploads yaml containing execution variable definitions to the backend.
   *
   * @param executionId ID of execution to which the variables belong.
   * @param files YAML defining the execution variables.
   * @returns Observable of the response.
   */
  postVariablesYaml(executionId: number, yaml: string): Observable<string> {
    return this.postVariablesFiles(executionId, [this._createFileFromVariables(yaml)]);
  }

  /**
   * Creates a form data object for a POST request
   * to upload execution variables.
   *
   * @param executionID ID of the plan execution.
   * @param files Array of inventory files.
   * @returns FormData object.
   */
  private _createFormData(executionID: number, files?: File[]): FormData {
    const formData = new FormData();
    formData.append('plan_execution_id', executionID.toString());

    files?.forEach((file, i) => {
      const fileName = 'inventory_' + i;
      formData.append(fileName, file, fileName);
    });

    return formData;
  }

  private _createFileFromVariables(variables: string): File {
    return new File([variables], 'inventory_file', { type: 'text/plain' });
  }
}
