import { LayoutModule } from '@angular/cdk/layout';
import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { Spied } from 'src/app/testing/utility/utility-types';
import { BackendStatusService } from '../http/backend-status/backend-status.service';
import { NavigationComponent } from './navigation.component';
import { ResponsiveSidenavDirective } from './responsive-sidenav/responsive-sidenav.directive';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  const isLive$ = new BehaviorSubject(true);
  const backendStatusStub = jasmine.createSpyObj('BackendStatusService', [], {
    isLive$: isLive$.asObservable()
  }) as Spied<BackendStatusService>;

  const testBackendStatus = (shouldBeLive: boolean): void => {
    const nativeEl = fixture.nativeElement as HTMLElement;
    const backendStatusContainer = nativeEl.querySelector('.backend-status');
    const dotSpan = backendStatusContainer.querySelector('.backend-status__dot');
    const statusText = backendStatusContainer.querySelector('.backend-status__status');

    const statusName = shouldBeLive ? 'live' : 'offline';
    expect(dotSpan.classList.contains('backend-status__dot--' + statusName)).toBeTrue();
    expect(statusText.textContent).toContain(statusName);
  };

  beforeEach(async () => {
    /**
     * Needs to override change detection strategy to Default
     * otherwise fixture.detectChanges() doesn't work properly.
     */
    TestBed.configureTestingModule({
      imports: [
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        LayoutModule,
        MatIconModule,
        BrowserAnimationsModule,
        MatMenuModule,
        MatSlideToggleModule,
        RouterTestingModule
      ],
      declarations: [NavigationComponent, ResponsiveSidenavDirective],
      providers: [{ provide: BackendStatusService, useValue: backendStatusStub }]
    })
      .overrideComponent(NavigationComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();

    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show that backend is live', () => {
    isLive$.next(true);
    fixture.detectChanges();

    testBackendStatus(true);
  });

  it('should show that backend is offline', () => {
    isLive$.next(false);
    fixture.detectChanges();

    testBackendStatus(false);
  });
});
