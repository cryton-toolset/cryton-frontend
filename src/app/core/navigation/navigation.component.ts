import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { delay, first, switchMapTo, takeUntil } from 'rxjs/operators';
import { ThemeService } from 'src/app/core/services/theme/theme.service';
import { renderComponentTrigger } from 'src/app/shared/animations/render-component.animation';
import { ComponentInputDirective } from 'src/app/shared/directives/component-input.directive';
import { BackendStatusService } from '../http/backend-status/backend-status.service';
import { metaRoutes, Route, routes } from './routes';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  animations: [
    trigger('toggleSublinks', [
      state('closed', style({ transform: 'scaleY(0)', height: '0' })),
      state('open', style({ transform: 'scaleY(1)', height: '*' })),
      transition('open <=> closed', animate('250ms ease'))
    ]),
    renderComponentTrigger
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild(ComponentInputDirective, { static: true }) alertHost!: ComponentInputDirective;

  routes: Route[] = routes;
  metaRoutes: Route[] = metaRoutes;

  openRoute: Route;
  selectedRoute: Route;

  isAccountClosed = true;
  loadingStatus$ = new BehaviorSubject<boolean>(false);
  isBackendLive = false;

  private _destroy$ = new Subject<void>();

  constructor(
    public themeService: ThemeService,
    private _backendStatus: BackendStatusService,
    private _cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this._backendStatus.isLive$.pipe(takeUntil(this._destroy$)).subscribe(isLive => {
      this.isBackendLive = isLive;
      this._cd.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  /**
   * Toggles visibility of route sublinks.
   *
   * @param route Route to be toggled.
   */
  toggleRoute(route: Route): void {
    if (this.openRoute === route) {
      this.openRoute = null;
    } else {
      this.openRoute = route;
    }
  }

  toggleDarkMode(e: MatSlideToggleChange): void {
    if (e.checked) {
      this.themeService.changeTheme('dark-theme', true);
    } else {
      this.themeService.changeTheme('light-theme', false);
    }
  }

  checkBackendStatus(): void {
    this.loadingStatus$.next(true);

    of({})
      .pipe(first(), delay(200), switchMapTo(this._backendStatus.checkBackendStatus().pipe(first())))
      .subscribe(() => this.loadingStatus$.next(false));
  }
}
