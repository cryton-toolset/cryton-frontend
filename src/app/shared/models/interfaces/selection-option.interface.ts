export interface SelectionOption<T> {
  value: T;
  name: string;
}
