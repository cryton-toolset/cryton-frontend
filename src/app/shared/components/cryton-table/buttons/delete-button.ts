import { MatDialog } from '@angular/material/dialog';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, mergeMap, tap } from 'rxjs/operators';
import { CrytonRESTApiService } from 'src/app/core/http/cryton-rest-api-service';
import { environment } from 'src/environments/environment';
import { CertainityCheckComponent } from '../../certainity-check/certainity-check.component';
import { HasID } from '../models/has-id.interface';
import { ApiActionButton } from './api-action-button';

export class DeleteButton<T extends HasID> extends ApiActionButton<T> {
  name = 'Delete';
  icon = 'delete';

  constructor(private _service: CrytonRESTApiService<T>, private _dialog: MatDialog) {
    super();
  }

  executeAction(row: T): Observable<string> {
    const dialogRef = this._dialog.open(CertainityCheckComponent);

    return dialogRef.afterClosed().pipe(
      mergeMap(res => {
        if (res) {
          this.addToLoading(row);
          return this._service.deleteItem(row.id).pipe(
            delay(environment.refreshDelay),
            tap(() => {
              this.removeFromLoading(row);
              this._deleted$.next();
            }),
            catchError(err => {
              this.removeFromLoading(row);
              return throwError(() => new Error(err));
            })
          );
        } else {
          return of(null) as Observable<string>;
        }
      })
    );
  }
}
