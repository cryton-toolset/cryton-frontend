import { WorkerInventoriesService } from 'src/app/core/http/worker-inventory/worker-inventories.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { CrytonTableDataSource } from 'src/app/shared/components/cryton-table/data-source/cryton-table.datasource';
import { Column } from '../models/column.interface';

export class WorkerInventoriesDataSource extends CrytonTableDataSource<Worker> {
  columns: Column[] = [
    {
      name: 'id',
      display: 'ID',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'state',
      display: 'STATE',
      highlight: true,
      filterable: true,
      sortable: true
    },
    {
      name: 'name',
      display: 'NAME',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'description',
      display: 'DESCRIPTION',
      highlight: false,
      filterable: true,
      sortable: true
    }
  ];
  displayFunctions: ((input: Worker) => string)[] = [null, null, null, null];
  highlightDictionary = {
    ready: 'blue',
    down: 'black',
    up: 'green'
  };

  constructor(protected dataService: WorkerInventoriesService) {
    super(dataService);
  }
}
