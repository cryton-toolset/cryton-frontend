import { PlanService } from 'src/app/core/http/plan/plan.service';
import { CrytonTableDataSource } from 'src/app/shared/components/cryton-table/data-source/cryton-table.datasource';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';
import { Plan } from '../../../../models/api-responses/plan.interface';
import { Column } from '../models/column.interface';

export class PlanTableDataSource extends CrytonTableDataSource<Plan> {
  columns: Column[] = [
    {
      name: 'id',
      display: 'ID',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'name',
      display: 'NAME',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'owner',
      display: 'OWNER',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'created_at',
      display: 'CREATED AT',
      highlight: false,
      filterable: false,
      sortable: true
    }
  ];
  displayFunctions = [null, null, null, (input: Plan): string => this._datePipe.transform(input.created_at), null];
  highlightDictionary = {};

  constructor(protected _planService: PlanService, private _datePipe: CrytonDatetimePipe) {
    super(_planService);
  }
}
