import { RunService } from 'src/app/core/http/run/run.service';
import { Run } from 'src/app/models/api-responses/run.interface';
import { CrytonTableDataSource } from 'src/app/shared/components/cryton-table/data-source/cryton-table.datasource';
import { CrytonDatetimePipe } from 'src/app/shared/pipes/cryton-datetime.pipe';
import { Column } from '../models/column.interface';

export class RunTableDataSource extends CrytonTableDataSource<Run> {
  columns: Column[] = [
    {
      name: 'id',
      display: 'ID',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'state',
      display: 'STATE',
      highlight: true,
      filterable: true,
      sortable: true
    },
    {
      name: 'schedule_time',
      display: 'SCHEDULE',
      highlight: false,
      filterable: false,
      sortable: true
    }
  ];
  displayFunctions: ((input: Run) => string)[] = [
    null,
    null,
    (input: Run): string => this._crytonDatetime.transform(input.schedule_time)
  ];
  highlightDictionary = {
    pending: 'pending',
    scheduled: 'pending',
    running: 'running',
    finished: 'success',
    pausing: 'stopping',
    paused: 'down',
    terminated: 'error'
  };

  constructor(protected runService: RunService, private _crytonDatetime: CrytonDatetimePipe) {
    super(runService);
  }
}
