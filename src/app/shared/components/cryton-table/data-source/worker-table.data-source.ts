import { WorkersService } from 'src/app/core/http/worker/workers.service';
import { Worker } from 'src/app/models/api-responses/worker.interface';
import { CrytonTableDataSource } from 'src/app/shared/components/cryton-table/data-source/cryton-table.datasource';
import { Column } from '../models/column.interface';

export class WorkerTableDataSource extends CrytonTableDataSource<Worker> {
  columns: Column[] = [
    {
      name: 'id',
      display: 'ID',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'state',
      display: 'STATE',
      highlight: true,
      filterable: true,
      sortable: true
    },
    {
      name: 'name',
      display: 'NAME',
      highlight: false,
      filterable: true,
      sortable: true
    },
    {
      name: 'description',
      display: 'DESCRIPTION',
      highlight: false,
      filterable: true,
      sortable: true
    }
  ];
  displayFunctions: ((input: Worker) => string)[] = [null, null, null, null];
  highlightDictionary = {
    ready: 'pending',
    down: 'down',
    up: 'running'
  };

  constructor(protected workersService: WorkersService) {
    super(workersService);
  }
}
