import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

type VerticalOrientation = 'top' | 'middle' | 'bottom';
type HorizontalOrientation = 'left' | 'middle' | 'right';
export type TooltipOrientation = [HorizontalOrientation, VerticalOrientation];

@Component({
  selector: 'app-cryton-tooltip',
  templateUrl: './cryton-tooltip.component.html',
  styleUrls: ['./cryton-tooltip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CrytonTooltipComponent {
  @Input() orientation: TooltipOrientation;
  @Input() x: number;
  @Input() y: number;

  constructor() {}
}
