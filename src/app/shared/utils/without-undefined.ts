export const withoutUndefinedAndNull = (object: object): object => {
  if (!object) {
    return object;
  }

  for (const key of Object.keys(object)) {
    const value = object[key];

    if (value === null || value === undefined || value === '') {
      delete object[key];
    } else if (isObject(value)) {
      object[key] = withoutUndefinedAndNull(value);

      if (Object.keys(object[key]).length === 0) {
        delete object[key];
      }
    } else if (Array.isArray(value)) {
      object[key] = value.map(item => withoutUndefinedAndNull(item));

      if (object[key].length === 0) {
        delete object[key];
      }
    }
  }

  return object;
};

const isObject = (value: unknown): boolean => {
  return typeof value === 'object' && !Array.isArray(value) && value !== null;
};
