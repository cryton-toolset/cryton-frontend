# Cryton Frontend
Cryton Frontend is a graphical interface used to interact with [Cryton Core](https://gitlab.ics.muni.cz/cryton/cryton-core) (its API).

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that **only 
the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/).

## Quick-start
Please keep in mind that [Cryton Core](https://gitlab.ics.muni.cz/cryton/cryton-core) must be running and its REST API must be reachable.

Make sure [Docker](https://docs.docker.com/engine/install/) is installed.
Optionally, check out these [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).

The following script hosts Cryton Frontend at http://localhost:8080/ using Docker. 
```shell
docker run -d -p 127.0.0.1:8080:80 registry.gitlab.ics.muni.cz:443/cryton/cryton-frontend
```

If necessary, update the settings at http://localhost:8080/app/user/settings.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/).

## Contributing
Contributions are welcome. Please **contribute to the [project mirror](https://gitlab.com/cryton-toolset)** on gitlab.com.
For more information see the [contribution page](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/contribution-guide/).
